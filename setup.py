from setuptools import find_packages, setup

setup(
    name='rayengine',
    version='0.0.1',
    description='A library to render 3D objects using raymarching',
    author='Baptiste Lambert',
    url='https://framagit.org/Blaireau/raymarching',
    packages=find_packages(),
)
