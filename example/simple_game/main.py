from rayengine.world import World
from rayengine.entities import Model, Light
from rayengine.mathtools import Vec3, Orientation

from colors import SKY_COLORS

import arcade
import random
from pathlib import Path
from math import floor, pi, sqrt


random.seed('mais')

SUN_COLOR = Vec3(2.1, 1.9, 1.7)
R_SUN_COLOR = Vec3(0.7833, 0.5662, 0.4063)
SKY_LIGHT_COLOR = Vec3(0.0, 0.1, 0.3)


CAMERA_MOVE = {
    'p': Vec3(0, 1, 0),
    'i': Vec3(0, -1, 0),
    'u': Vec3(-1, 0, 0),
    'e': Vec3(1, 0, 0),
    'o': Vec3(0, 0, 1),
    'é': Vec3(0, 0, -1)
}

SUN_MOVE = {
    'd': 1,
    's': -1
}
SUN_ANGULAR_SPEED = 0.1

TEST_KEY = {
    'l': 1,
    'v': -1
}
TEST_PARAM_SPEED = 1

SPEED = 100
GSPEED = 500

BOOST_KEY = 'space'
PRINT_KEY = 'n'

DEBUG = list(range(1, 8))

RENDER_MODE = False

def random_color():
    return Vec3(*[random.randint(0, 100) / 100 for _ in range(3)])

class MyGame(World):

    def __init__(self):
        super().__init__()
        self.render.set_fullscreen(True)
        if RENDER_MODE:
            self.timeBuffer = 0
            self.render.set_visible(False)
            self.frame = -2
            self.frameMax = 12*24
            self.render.debugMode = False
        self.mountain = Model('montain')
        self.mountain.attach_to(self.origin)
        self.sun = Light(SUN_COLOR, keylight=True)
        self.skylight = Light(SKY_LIGHT_COLOR)
        self.reflectedSun = Light(R_SUN_COLOR)
        self.add_light(self.sun)
        self.add_light(self.skylight)
        self.add_light(self.reflectedSun)
        self.sun.orientation = Orientation(Vec3(1, 0, 0))
        self.reflectedSun.orientation = Orientation(Vec3(1, 0, 0))
        self.sunAngle = 1
        self.sunAngularSpeed = 0

        self.tree = Model('tree')
        self.tree.attach_to(self.origin)
        self.tree.pos = Vec3(-12600, 15280, 140)

        self.camera.pos = Vec3(-12700, 15380, 140)
        self.camera.orientation = (  Orientation(Vec3(0, 0, 1), angle=-2.97)
                                   & Orientation(Vec3(1, 0, 0), angle=-0.02))
        self.directions = {key:False for key in CAMERA_MOVE.keys()}
        self.rotation = False
        self.debugMode = 0
        self.boost = False
        self.testParam = 12
        self.testParamSpeed = 0

    def print_info(self):
        print(self.camera.speed.norm())

    def sun_curve(self, time):
        riseTime = 7
        setTime = 20
        if time < riseTime or time > setTime:
            return pi
        return pi*((time-riseTime)/(setTime-riseTime)-0.5)

    def update_camera_movement(self):
        speed = Vec3(0, 0, 0)
        for key, direction in CAMERA_MOVE.items():
            if self.directions[key]:
                speed += direction
        if self.boost:
            speed *= GSPEED
        else:
            speed *= SPEED
        self.camera.speed = ( self.camera.xaxis*speed.x
                             +self.camera.yaxis*speed.y
                             +self.camera.zaxis*speed.z)

    def on_update(self, dt):
        self.update_camera_movement()
        self.camera.update(dt)
        self.sunAngle = self.sun_curve(self.testParam)
        self.testParam += self.testParamSpeed*TEST_PARAM_SPEED*dt
        self.testParam = self.testParam % 24
        axis = Vec3(1, 0, 0)
        self.sun.orientation = (  Orientation(axis, angle=self.sunAngle)
                                & Orientation(Vec3(0, 1, 0), angle=2/4))
        if abs(self.sunAngle) > pi/2.0:
            self.reflectedSun.color = Vec3(0, 0, 0)
        else:
            self.reflectedSun.color = R_SUN_COLOR
            self.reflectedSun.orientation = (  Orientation(axis, angle=-self.sunAngle)
                                             & Orientation(Vec3(0, 1, 0), angle=-2/4))
        self.render.set_test_param(self.testParam)

        # compute sky color illuminating the mountain
        hour = floor(self.testParam)
        minutes = self.testParam % 1.0
        prevcolors = SKY_COLORS[hour]
        nextcolors = SKY_COLORS[(hour+1)%24]
        colors = [n*minutes + p*(1-minutes) for p, n in zip(prevcolors, nextcolors)]
        skylightcolor = (colors[0] + colors[1])*0.5
        self.skylight.color = skylightcolor
        self.render.set_sky_data(*colors)

        if RENDER_MODE:
            self.testParam = 24*self.frame/self.frameMax
            self.timeBuffer += dt
            if self.timeBuffer > 2.0:
                self.timeBuffer = 0
                if self.frame >= 0:
                    image = arcade.get_image(0, 0, *self.render.get_size())
                    image.save(f"output/{self.frame}.png")
                    print(f'Generating frame {self.frame}')
                self.frame += 1
                if self.frame >= self.frameMax:
                    self.render.close()

    def on_key_press(self, key, modifiers):
        if key == PRINT_KEY:
            self.print_info()
        if key == BOOST_KEY:
            self.boost = not self.boost
        for testKey, direction in TEST_KEY.items():
            if key == testKey:
                self.testParamSpeed = direction
        for camKey in CAMERA_MOVE.keys():
            if key == camKey:
                self.directions[camKey] = True
        for sunKey, value in SUN_MOVE.items():
            if key == sunKey:
                self.sunAngularSpeed = value
        for debug_key in DEBUG:
            if key == str(debug_key):
                self.debugMode = 0 if self.debugMode == debug_key else debug_key
                self.render.set_debug_mode(self.debugMode)

    def on_key_release(self, key, modifiers):
        for camKey in CAMERA_MOVE.keys():
            if key == camKey:
                self.directions[camKey] = False
        for testKey in TEST_KEY.keys():
            if key == testKey:
                self.testParamSpeed = 0
        for sunKey in SUN_MOVE.keys():
            if key == sunKey:
                self.sunAngularSpeed = 0



game = MyGame()
game.start()
