import time
import subprocess
from pathlib import Path

IMAGE_DIR = Path(__file__).parent / 'wallpaper'
FRAME_PERIOD = 5 # nb of minutes between two frames
DELTA_TIME = FRAME_PERIOD*60


def main():
    while True:
        now = time.localtime()
        minutes = now.tm_hour*60 + now.tm_min
        frame = minutes // FRAME_PERIOD
        image = IMAGE_DIR / f'{frame}.png'
        subprocess.run(['nitrogen', '--set-auto', image])
        time.sleep(DELTA_TIME)

if __name__ == '__main__':
    main()
