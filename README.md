# Raymarching

A raymarching implementation in python.

# Installation

- Clone the repository

# Setup

- Run the `setup` script in `example/simple_game`
This creates a virtual environment with arcade and the rayengine installed.
You need to run this script only the first time.

# Running

- Run the `run` script in `example/simple_game`
This will build the models and run the game.


