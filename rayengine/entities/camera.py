from .entity import Entity
from rayengine.mathtools import Vec3, Orientation

DEFAULT_SENSITIVITY = 0.002


class Camera(Entity):

    def __init__(self):
        super().__init__()
        self.nodetype = 'Camera'
        self.sensitivity = DEFAULT_SENSITIVITY

    def update_rotation(self, dx, dy):
        delta_rot = (  Orientation(Vec3(0, 0, 1), angle=-dx*self.sensitivity)
                     & Orientation(self.xaxis, angle=dy*self.sensitivity))
        self.orientation = self.orientation & delta_rot

    def get_data(self):
        return {
            'cameraPos':   self._global_pos.get_val(),
            'cameraDir':   self.get_global_coord(Vec3(0, 1, 0), True).get_val(),
            'cameraUp':    self.get_global_coord(Vec3(0, 0, 1), True).get_val(),
            'cameraRight': self.get_global_coord(Vec3(1, 0, 0), True).get_val(),
        }

