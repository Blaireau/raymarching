from rayengine.mathtools import Vec3
from rayengine.mathtools.localframe import LocalFrame


class Entity:

    __entity_count = 0

    def __init__(self, pos=None):
        self.__set_new_id()
        # print(f'New entity {self._id}')
        self._parent = None
        self._root = self
        self.nodetype = None
        self._childrens = set()
        self.localframe = LocalFrame(pos)

    def __set_new_id(self):
        Entity.__entity_count += 1
        self._id = Entity.__entity_count

    def get_childrens(self):
        return self._childrens

    def compute_render_data(self):
        self._global_pos = self.get_global_coord(Vec3(0, 0, 0))

    def get_global_coord(self, vec, orientationMode=False):
        local_vec = self.localframe.to_parent_coord(vec, orientationMode)
        if self._parent:
            return self._parent.get_global_coord(local_vec, orientationMode)
        else:
            return local_vec

    def __del__(self):
        pass
        # print(f'Removed entity {self._id}')

    def __repr__(self):
        return self.display(0)

    def display(self, level):
        parent = self._parent._id if self._parent is not None else None
        root = self._root._id if self._root is not None else None
        ent_type = type(self).__name__
        self_disp = f'{ent_type} {self._id}: [{parent}] {root}'
        display_lines = ['   '*level + self_disp]
        if self._childrens:
            display_lines.extend((c.display(level+1) for c in self._childrens))
        return '\n'.join(display_lines)

    def attach_to(self, parentEntity):
        if self._parent:
            self.detach()
        if parentEntity._root is self:
            raise Exception('The render tree can not contain cycles !\n'
                            'Make sure you are not attaching an entity to one of it’s childrens.'
                           )
        self._parent = parentEntity
        self._parent._add_child(self)
        self._set_root(self._parent._root)

    def detach(self):
        self._parent._remove_child(self)
        self._parent = None
        self._set_root(self)

    def _set_root(self, root):
        self._root = root
        for child in self._childrens:
            child._set_root(root)

    def destroy(self):
        if self._parent:
            self.detach()
        self._destroy()

    def _destroy(self):
        """destroy without removing yourself from your parent data"""
        del self._parent
        del self._root
        del self.localframe
        for child in self._childrens:
            child._destroy()
        del self._childrens

    def get_data(self):
        return {
            'pos':   self._global_pos.get_val(),
            'dir':   self.get_global_coord(Vec3(0, 1, 0), True).get_val(),
            'up':    self.get_global_coord(Vec3(0, 0, 1), True).get_val(),
            'right': self.get_global_coord(Vec3(1, 0, 0), True).get_val(),
        }

    @property
    def pos(self):
        return self.localframe.origin

    @pos.setter
    def pos(self, value):
        self.localframe.origin = value

    @property
    def speed(self):
        return self.localframe.speed

    @speed.setter
    def speed(self, value):
        self.localframe.speed = value

    @property
    def orientation(self):
        return self.localframe.orientation

    @orientation.setter
    def orientation(self, value):
        self.localframe.orientation = value

    @property
    def rotation(self):
        return self.localframe.rotation

    @rotation.setter
    def rotation(self, value):
        self.localframe.rotation = value

    @property
    def angular_speed(self):
        return self.localframe.angular_speed

    @angular_speed.setter
    def angular_speed(self, value):
        self.localframe.angular_speed = value

    @property
    def xaxis(self):
        return self.localframe.xaxis

    @property
    def yaxis(self):
        return self.localframe.yaxis

    @property
    def zaxis(self):
        return self.localframe.zaxis

    def update(self, dt):
        self.localframe.update(dt)

    def _remove_child(self, child):
        try:
            self._childrens.remove(child)
        except KeyError:
            raise KeyError(f'{self} has no child {child}')

    def _add_child(self, child):
        self._childrens.add(child)

