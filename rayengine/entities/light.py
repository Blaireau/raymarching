from .entity import Entity
from rayengine.mathtools import Vec3

class Light(Entity):

    def __init__(self, color, *, keylight=False):
        self.color = color
        self.isKeyLight = keylight
        super().__init__()

    @property
    def direction(self):
        return self.orientation.rotate_vector(Vec3(0, 0, 1))

    def get_data(self):
        return (*self.color.get_val(), 0.0,
                *self.direction.get_val(), 0.0,
                self.isKeyLight, 0, 0, 0)

