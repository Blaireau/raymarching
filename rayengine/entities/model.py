from .entity import Entity
from rayengine.mathtools import Vec3

class Model(Entity):

    MODEL_DATABASE = None

    @classmethod
    def load_from_database(cls, name):
        try:
            return cls.MODEL_DATABASE[name]
        except KeyError:
            raise KeyError(f"Could not find the model '{name}'.")

    def __init__(self, name):
        super().__init__()
        self.nodetype = 'Model'
        data = Model.load_from_database(name)

        self.sdfID = data['index']
        self.boundType = data['boundType']
        self.boundValue = data['boundValue']

        # self._color = Vec3(0.2, 0, 0)

    # @property
    # def color(self):
        # return self._color * 5

    # @color.setter
    # def color(self, value):
        # self._color = value / 5

    def get_model_data(self):
        model = (
             *self._global_pos.get_val(), self.boundValue,
             0.0, 0.0, 0.0, 0.0,
             self.sdfID, self.boundType, 0, 0
        )
        return model

