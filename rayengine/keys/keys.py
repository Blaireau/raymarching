from .keys_table import KEY_NAMES


def get_key_name(key):
    try:
        key_name = KEY_NAMES[key]
    except KeyError:
        key_name = str(key)
    return key_name
