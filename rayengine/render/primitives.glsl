vec3 Point3D(float x, float y, float z){
    return vec3(x, y, z);
}

float unitsmooth(float x){
    return min(1, max(0, 3*x*x - 2*x*x*x));
}

float unitsmoothgrad(float x){
    return 6*x*(1-x);
}

float mynoise(float x, float y){
    // gives random number in range [-1; 1]
    float u = 83*fract(x / 3.1415926535);
    float v = 29*fract(y / 13.783923478238);
    float r = fract(u*(u+v)+v);
    return 1-2*r;
}

float is_in_square(vec2 np, vec3 point, vec3 dir, float wavelength){
    float xi = floor(np.x);
    float yi = floor(np.y);
    float A = mynoise(xi, yi);
    float B = mynoise(xi + 1, yi);
    float C = mynoise(xi, yi + 1);
    float D = mynoise(xi + 1, yi + 1);
    float hmax = max(A, max(B, max(C, D)));

    if (point.z < hmax){
        return 0.1;
    }

    float dist_plane = (hmax - point.z) / dir.z;

    vec2 u = np - vec2(xi, yi);
    float ax = 0;
    if (dir.x > 0){
        ax = 1;
    }
    float ay = 0;
    if (dir.y > 0){
        ay = 1;
    }
    float t1 = ((-u.x + ax) / dir.x);
    float t3 = ((-u.y + ay) / dir.y);
    float dt = wavelength*min(t1, t3);
    if (dir.z < 0){
        return 0.1+min(dt, dist_plane);
    } else {
        return 0.1+min(dt, MAX_RENDER_DIST);
    }
    return 0.1+dist_plane;
}


float octave(vec2 np){
    float xi = floor(np.x);
    float yi = floor(np.y);
    float A = mynoise(xi, yi);
    float B = mynoise(xi + 1, yi);
    float C = mynoise(xi, yi + 1);
    float D = mynoise(xi + 1, yi + 1);
    float sx = unitsmooth(np.x - xi);
    float sy = unitsmooth(np.y - yi);
    float f = (A + (B - A)*sx
                 + (C - A)*sy
                 + (A + D - B - C)*sx*sy);
    return f;
}

vec2 octave_grad(vec2 np){
    float xi = floor(np.x);
    float yi = floor(np.y);
    float A = mynoise(xi, yi);
    float B = mynoise(xi + 1, yi);
    float C = mynoise(xi, yi + 1);
    float D = mynoise(xi + 1, yi + 1);
    float sx = unitsmooth(np.x - xi);
    float sy = unitsmooth(np.y - yi);
    float sxg = unitsmoothgrad(np.x - xi);
    float syg = unitsmoothgrad(np.y - yi);
    vec2 grad = vec2((B - A)*sxg + (A + D - B - C)*sxg*sy, 
                     (C - A)*syg + (A + D - B - C)*sx*syg);
    return grad;
}

PointOnRay Union(PointOnRay pa, PointOnRay pb){
    if (pa.dist < pb.dist){
        return pa;
    } else {
        return pb;
    }
}

PointOnRay Sphere(vec3 point, int matID, float radius){
    float dist = length(point) - radius;
    return PointOnRay(matID, dist, point); 
}

PointOnRay Box(vec3 point, int matID, float w, float l, float h){
    vec3 box = vec3(w/2, l/2, h/2);
    vec3 q = abs(point) - box;
    float dist = length(max(q, 0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
    return PointOnRay(matID, dist, point);
}

PointOnRay Cylinder(vec3 point, int matID, float height, float radius) {
  vec2 d = abs(vec2(length(point.xy),point.z)) - vec2(radius,height);
  float dist = min(max(d.x,d.y),0.0) + length(max(d,0.0));
  return PointOnRay(matID, dist, point);
}

PointOnRay Cone(vec3 point, int matID, float sinC, float cosC, float height) {
    // c is the sin/cos of the angle
    vec2 c = vec2(sinC, cosC);

    vec2 q = height*vec2(c.x/c.y,-1.0);

    vec2 w = vec2( length(point.xy), point.z );
    vec2 a = w - q*clamp( dot(w,q)/dot(q,q), 0.0, 1.0 );
    vec2 b = w - q*vec2( clamp( w.x/q.x, 0.0, 1.0 ), 1.0 );
    float k = sign( q.y );
    float d = min(dot( a, a ),dot(b, b));
    float s = max( k*(w.x*q.y-w.y*q.x),k*(w.y-q.y)  );
    float dist = sqrt(d)*sign(s);
    return PointOnRay(matID, dist, point);
}
float montain_noise(vec2 projp, int octaveLevel){
    mat2 Ma = mat2(4.0/5.0, 3.0/5.0, -3.0/5.0, 4.0/5.0);
    float noise = 0.0;
    float scale = 2.0;
    for (int i = 0; i < 15; i++){
        if ( i > octaveLevel){
            return noise;
        }
        noise += octave(projp) / scale;
        projp = 2*Ma*projp;
        scale = 2.0*scale;
    }
    return noise;
}

vec2 montain_grad(vec2 projp, int octaveLevel){
    mat2 Ma = mat2(4.0/5.0, 3.0/5.0, -3.0/5.0, 4.0/5.0);
    mat2 Mb = mat2(1, 0, 0, 1);
    vec2 grad = vec2(0.0);
    for (int i = 0; i < 15; i++){
        if ( i > octaveLevel){
            return grad;
        }
        grad += Mb*octave_grad(projp) / 2.0;
        projp = 2*Ma*projp;
        Mb = Mb*Ma;
    }
    return grad;
}

PointOnRay PiecewiseHeightmap(vec3 point, int matID, float camDist, Ray ray, float amplitude, float wavelength){
    vec2 projp = point.xy / wavelength;

    float f = amplitude*montain_noise(projp, 6);
    float height = point.z - f;

    // distance to bounding plane
    if (camDist < 0 || height < 0){
        f = amplitude*montain_noise(projp, 10);
        height = point.z - f;
        return PointOnRay(matID, height, point);
    }

    if (point.z > amplitude){
        float dist = (amplitude - point.z) / ray.direction.z;
        if (ray.direction.z > 0){
            dist = 10000;
        }
        return PointOnRay(matID, 1 + dist, point);
    } else {
        float k = 2; // lipschitz constant at z = 1/2
        float kmin = 0.0;
        float z = ((point.z / amplitude) + 1)/2.0;
        float lambda = 4*k*z*(1-z) + kmin;
        float dxy = sqrt(1 - ray.direction.z * ray.direction.z);
        float dist = 0.5*height / (abs(ray.direction.z) + lambda*dxy);
        dist = max(dist, 0.1);
        return PointOnRay(matID, dist, point);
    }
}


PointOnRay Forest(vec3 point, int matID, float cellSize){
    return PointOnRay(matID, cellSize, point);
}
/* PointOnRay Forest(vec3 point, int matID, float cellSize){ */
/*     vec2 projpMount = point.xy / 3000.0; */

/*     float f = 2000.0*montain_noise(projpMount, 4); */
/*     point.z -= f; */
/*     vec3 projp = point / cellSize; */
/*     vec2 cell = floor(projp.xy); */

/*     float dist = min(min(min( */
/*                        random_sphere(projp, cell + vec2(0, 0)), */
/*                        random_sphere(projp, cell + vec2(0, 1))), */
/*                        random_sphere(projp, cell + vec2(1, 0))), */
/*                        random_sphere(projp, cell + vec2(1, 1))); */
/*     return PointOnRay(matID, dist*cellSize, point); */
/* } */


float smin( float a, float b, float k ){
    float h = max( k-abs(a-b), 0.0 )/k;
    return min( a, b ) - h*h*k*(1.0/4.0);
} 

float smax( float d1, float d2, float k ) {
    float h = clamp( 0.5 - 0.5*(d2-d1)/k, 0.0, 1.0 );
    return mix( d2, d1, h ) + k*h*(1.0-h);
}

float sdCone(vec3 p, vec2 c) {
    // c is the sin/cos of the angle
    vec2 q = vec2( length(p.xy), -p.z );
    float d = length(q-c*max(dot(q,c), 0.0));
    return d * ((q.x*c.y-q.y*c.x<0.0)?-1.0:1.0);
}


/* PointOnRay SphereNoiseMountain(vec3 point, int matID, float camDist, float amplitude){ */

/*     float dist = point.z; */

/*     float mount = dist; */
/*     for (int i = 1; i < 20; i++){ */
/*         float x = mod(32935*i, 10000); */
/*         float y = mod(58392*i, 10000); */
/*         float h = 500+mod(49352*i, 1500); */
/*         float cone = sdCone(point - vec3(x, y, h), vec2(sqrt(2)/2.0, sqrt(2)/2.0)); */
/*         mount = smin(mount, cone, 200); */
/*     } */
/*     dist = mount; */

/*     /1* float s = amplitude; *1/ */
/*     /1* vec3 np = point; *1/ */
/*     /1* for (int o = 1; o < 5; o++){ *1/ */

/*     /1*     for (int c = 1; c < 3; c++){ *1/ */
/*     /1*         float n = s*sphere_noise(np/s); *1/ */
/*     /1*         n = smax(n, dist - 0.05*s, 0.1*s); *1/ */
/*     /1*         dist = smin(n, dist, 0.05*s); *1/ */

/*     /1*         np += vec3(0, 0, s*0.5); *1/ */
/*     /1*         np = mat3(4.0/5.0, 3.0/5.0, 0, *1/ */
/*     /1*                   -3.0/5.0, 4.0/5.0, 0, *1/ */
/*     /1*                   0, 0, 1)*np; *1/ */
/*     /1*     } *1/ */

/*     /1*     s = 0.5*s; *1/ */
/*     /1* } *1/ */
/*     return PointOnRay(matID, dist, point); */

/* } */

/* vec4 planeSDF(vec3 point, vec3 center, vec4 data, vec3 color){ */
/*     vec3 n = data.xyz; */
/*     float dist = dot(point - center, n); */ 
/*     return vec4(color, dist); */
/* } */

/* vec4 torusSDF(vec3 point, vec3 torusPos, float ra, float rb, vec3 color){ */
/*     vec3 relP = point - torusPos; */

/*     vec3 rotP = rotate(relP, vec3(0, 0, 1), iTime); */ 
/*     rotP = rotate(rotP, vec3(1, 0, 0), 0.7); */ 

/*     vec2 q = vec2(length(rotP.xz)-ra,rotP.y); */
/*     /1* vec2 q = vec2(length(relP.xz)-ra,relP.y); *1/ */
/*     return vec4(color, length(q)-rb); */
/* } */

/* vec4 planeSDF(vec3 point, vec3 normal, float height, vec3 color){ */
/*   // n must be normalized */
/*   return vec4(color, dot(point,normal) + height); */
/* } */
