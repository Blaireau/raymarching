import arcade
from math import ceil

from .shader import RenderShader, AntiAliasingShader, UpscaleShader
from rayengine.mathtools import Vec3
from rayengine.perfs import PerfMeter
from rayengine.keys import get_key_name

import time


DEBUG = False

SCREEN_WIDTH = 1920
SCREEN_HEIGHT = 1080
# SCREEN_WIDTH //= 4
# SCREEN_HEIGHT //= 4
# SCREEN_WIDTH = 500
# SCREEN_HEIGHT = 500

UPSCALE_FACTOR = 1
# UPSCALE_FACTOR = 4


DTVEC_SIZE = 20

SCREEN_TITLE = "Raymarching game engine"

debug = print if DEBUG else lambda *args, **kwargs: None


class Render(arcade.Window):

    def __init__(self, userSource):
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
        self.ctx.disable(self.ctx.BLEND)
        self.set_exclusive_mouse(True)
        self.set_mouse_visible(False)
        self.time = 0
        self.debugMode = True
        w, h = [int(s // UPSCALE_FACTOR) for s in [SCREEN_WIDTH, SCREEN_HEIGHT]]
        self.renderShader = RenderShader(w, h, userSource)
        self.aaShader = AntiAliasingShader(w, h)
        self.upscaleShader = UpscaleShader(SCREEN_WIDTH, SCREEN_HEIGHT, UPSCALE_FACTOR)
        self.world = None
        self.frame_perf = PerfMeter('Fps: {fps} [{value_ms}/16 ms]')
        self.tree_perf = PerfMeter('Render tree: {value_ms}ms')
        self.draw_perf = PerfMeter('Drawing: {value_ms}ms')
        names = ['fps', 'draw_time', 'tree_time', 'position', 'testParam']
        self.text = {}
        for i, name in enumerate(names):
            t = arcade.Text('', start_x=0.0, start_y=SCREEN_HEIGHT*(0.9-0.05*i),
                            color=arcade.color.WHITE, font_size=15)
            self.text[name] = t


    def start(self, world):
        self.world = world
        self.run()

    def send_event(self, event, *args, **kwargs):
        if self.world:
            callback = getattr(self.world, event)
            callback(*args, **kwargs)

    def set_debug_mode(self, value):
        self.renderShader.set_debug_mode(value)
        self.aaShader.set_debug_mode(value)
        # self.upscaleShader.set_debug_mode(value)

    def set_sky_data(self, skydark, skylite):
        self.renderShader.set_uniform('skydark', skydark.get_val())
        self.renderShader.set_uniform('skylite', skylite.get_val())

    def set_test_param(self, value):
        self._testParamValue = round(value, 5)
        self.renderShader.set_uniform('testParam', value)
        self.renderShader.set_uniform('iTime', value)

    def on_draw(self):
        self.tree_perf.start_stopwatch()
        # tree computations
        models = self.world.get_geometry()
        self.renderShader.update_models(models)
        cameraData = self.world.camera.get_data()
        self.renderShader.update_camera(cameraData)
        lightsData = self.world.get_lights_data()
        self.renderShader.update_lights(lightsData)
        self.tree_perf.stop_stopwatch()
        self.draw_perf.push_dt(self.frame_perf.last_dt - self.tree_perf.last_dt)
        # shader computations
        self.clear()
        # render pass
        self.renderShader.use()
        self.renderShader.draw()
        # anti aliasing pass
        self.aaShader.use()
        self.aaShader.draw()
        # upscale pass
        self.use()
        self.upscaleShader.draw()

        self.text['fps'].text = str(self.frame_perf)
        self.text['draw_time'].text = str(self.draw_perf)
        self.text['tree_time'].text = str(self.tree_perf)
        self.text['position'].text = f'Pos: {self.world.camera.pos}'
        self.text['testParam'].text = f'Test: {self._testParamValue}'
        if self.debugMode:
            for textobj in self.text.values():
                textobj.draw()
        self.ctx.disable(self.ctx.BLEND)


    def on_update(self, dt):
        self.time += dt
        self.frame_perf.push_dt(dt)
        self.renderShader.set_uniform('iTime', self.time)
        self.send_event('on_update', dt)

    def on_key_press(self, key, modifiers):
        if key == arcade.key.Q:
            self.close()
        key_name = get_key_name(key)
        self.send_event('on_key_press', key_name, modifiers)

    def on_key_release(self, key, modifiers):
        key_name = get_key_name(key)
        self.send_event('on_key_release', key_name, modifiers)

    def on_mouse_motion(self, *args):
        self.send_event('on_mouse_motion', *args)

