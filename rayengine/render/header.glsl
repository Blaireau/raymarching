#version 430

uniform vec2 iResolution;
uniform float iTime;

uniform vec3 skylite;
uniform vec3 skydark;

uniform vec3 cameraPos;
uniform vec3 cameraDir;
uniform vec3 cameraUp;
uniform vec3 cameraRight;

uniform int lightCount;

uniform float testParam;

uniform int DEBUG_LAYER;

struct ModelData {
    vec4 posData;
    vec4 orientData;
    ivec4 indexData;
};

struct LightData{
    vec4 colorData;
    vec4 orientData;
    ivec4 indexData;
};

struct Model {
    vec3 pos;
    float boundValue;
    vec4 orientation;
    int sdfID;
    int boundType;
};

struct Material {
    float metalic;
    vec3 color;
};

struct InputPoint {
    vec3 pos;
    float dist; 
};

struct PointOnRay {
    int materialID;
    float dist; 
    vec3 localPoint;
};

layout(std430, binding=0) readonly buffer ModelBuffer{
    ModelData modlist[];
};

layout(std430, binding=1) readonly buffer LightBuffer{
    LightData lightlist[];
};

in vec2 uv;

out vec4 fragColor;


// maths
#define PI 3.1415926535

// colors
#define GRID_SIZE 0.1
#define LINE_WIDTH 0.005

#define BLACK vec3(0, 0, 0)
#define WHITE vec3(1.0, 1.0, 1.0)
#define RED vec3(1.0, 0, 0)
#define GREEN vec3(0, 1.0, 0)
#define YELLOW vec3(1.0, 1.0, 0)
#define BLUE vec3(0, 0, 1.0)
#define GREEN vec3(0, 1.0, 0)
#define ORANGE vec3(1.0, 0.5, 0)
#define GREY vec3(0.5, 0.5, 0.5)


//rendering parameters 
#define SCREEN_DISTANCE 4
#define MAX_ITER 400
#define MAX_RENDER_DIST 20000.0
#define EPSILON 0.001
#define MAX_MODELS_ON_RAY 20

#define NORMALS_EPSILON 0.05
#define SHADOW_MAX_ITER 200
#define SHADOW_MAX_RENDER_DIST 5000.0
#define SHADOW_START_DIST 1 // must be much bigger than epsilon
#define SOFT_SHADOW_RADIUS_INC 5.0
#define SOFT_SHADOW_FACTOR 24

// bounds
#define SPHERE_BOUND_TYPE 0
#define PLANE_BOUND_TYPE 1

//debug parameters
#define ITER_XLOW 25 
#define ITER_LOW 50
#define ITER_MEDIUM 100
#define ITER_HIGH 200
#define ITER_GHIGH 300


#define UNIT_X vec3(1, 0, 0)
#define UNIT_Y vec3(0, 1, 0)
#define UNIT_Z vec3(0, 0, 1)

struct Ray{
    vec3 origin;
    vec3 direction;
    int [MAX_MODELS_ON_RAY] modelsOnRay;
    int modelCount;
};

struct Light{
    vec3 color;
    vec3 direction;
    int isKeyLight;
};
