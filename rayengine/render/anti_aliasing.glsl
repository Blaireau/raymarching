#version 430


// tutorial about anti-aliasing with FXAA
// https://catlikecoding.com/unity/tutorials/custom-srp/fxaa/

uniform float upscale_factor;
uniform vec2 iResolution;
uniform sampler2D mytexture;

uniform int DEBUG_LAYER;

in vec2 uv;

out vec4 fragColor;

#define BLACK vec3(0, 0, 0)
#define WHITE vec3(1, 1, 1)

#define BIAS 0.2


void add_to_blend_color(inout vec3 blendColor, inout float bestDelta,
                        vec3 pixelColor, float pixelDist, vec4 otherSample){
    /* float dist = otherSample.w; */
    vec3 otherColor = otherSample.rgb;
    /* vec3 deltaColor = abs(otherColor - pixelColor); */
    /* float delta_c = deltaColor.r + deltaColor.g + deltaColor.b; */ 
    /* float delta_d = step(0.1, abs(dist - pixelDist)/pixelDist); */
    float factor = BIAS + length(otherColor - pixelColor);
    blendColor += otherColor*factor;
    bestDelta += factor;
    /* if (delta_d > bestDelta && delta_c > 0.1){ */
    /*     bestDelta = delta_d; */
    /*     blendColor = otherColor; */
    /* } */
} 


void main(){

    vec2 screen_pos = (2*uv-1);
    screen_pos.x = screen_pos.x * iResolution.x / iResolution.y;

    /* float pre = 100000.0; */ 
    /* float r = 1.0; */
    /* float deltax = r*ceil(upscale_factor/iResolution.x*pre)/pre; */ 
    /* float deltay = r*ceil(upscale_factor/iResolution.y*pre)/pre; */ 
    float deltax = 1.0 / iResolution.x * 1.0;
    float deltay = deltax;

    fragColor = vec4(BLACK, 1.0);

    vec4 a = texture(mytexture, uv);

    vec3 blendColor = vec3(0);
    float bestDelta = 0;
    vec3 pixelColor = a.rgb;
    float pixelDist = a.w;

    /* for (int i = -1; i <= 1; i++){ */
    /*     for (int j = -1; i <= 1; i++){ */
    /*         vec4 b = texture(mytexture, vec2(i*deltax+uv.x, j*deltay+uv.y)); */
    /*         add_to_blend_color(blendColor, bestDelta, pixelColor, pixelDist, b); */
    /*     } */
    /* } */
    vec4 b = texture(mytexture, vec2(uv.x, deltay+uv.y));
    vec4 c = texture(mytexture, vec2(uv.x, uv.y-deltay));
    vec4 d = texture(mytexture, vec2(uv.x+deltax, uv.y));
    vec4 e = texture(mytexture, vec2(uv.x-deltax, uv.y));
    vec4 f = texture(mytexture, vec2(uv.x+deltax, deltay+uv.y));
    vec4 g = texture(mytexture, vec2(uv.x-deltax, uv.y-deltay));
    vec4 h = texture(mytexture, vec2(uv.x+deltax, uv.y-deltay));
    vec4 i = texture(mytexture, vec2(uv.x-deltax, uv.y+deltay));

    add_to_blend_color(blendColor, bestDelta, pixelColor, pixelDist, b);
    add_to_blend_color(blendColor, bestDelta, pixelColor, pixelDist, c);
    add_to_blend_color(blendColor, bestDelta, pixelColor, pixelDist, d);
    add_to_blend_color(blendColor, bestDelta, pixelColor, pixelDist, e);
    add_to_blend_color(blendColor, bestDelta, pixelColor, pixelDist, f);
    add_to_blend_color(blendColor, bestDelta, pixelColor, pixelDist, g);
    add_to_blend_color(blendColor, bestDelta, pixelColor, pixelDist, h);
    add_to_blend_color(blendColor, bestDelta, pixelColor, pixelDist, i);

    /* float blendFactor = 0.5*smoothstep(0.01, 0.1, bestDelta); */
    /* float blendFactor = 0.6*smoothstep(0.01, 0.99, bestDelta); */
    blendColor = blendColor / bestDelta;
    if (DEBUG_LAYER == 0){
        fragColor.rgb = (a.rgb + blendColor) / 2.0;
    } else {
        fragColor.rgb = a.rgb;
    }
    /* float blendFactor = bestDelta; */

    /* fragColor.rgb = a.rgb; */
    /* if (bestDelta > 0.5){ */
    /*     /1* fragColor.rgb = (a.rgb + blendColor) / 2.0;//mix(a.rgb, blendColor/4, 0.5); *1/ */
    /*     fragColor.rgb = blendColor; */
    /*     if (DEBUG_LAYER == 7){ */
    /*         fragColor.rgb = vec3(0, 1, 0); */
    /*     } */
    /* } */
    if (DEBUG_LAYER == 6){
        fragColor.rgb = a.rgb;
    }
    if (DEBUG_LAYER == 7){
        fragColor.rgb = blendColor;
    }
    if (DEBUG_LAYER == 7){
        fragColor.rgb = mix(BLACK, WHITE, pixelDist);
    }

}
