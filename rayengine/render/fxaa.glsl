#version 430

// tutorial about anti-aliasing with FXAA
// https://catlikecoding.com/unity/tutorials/custom-srp/fxaa/

#define LUMA_ABS_THRESHOLD 0.0433 // default 0.0833
#define LUMA_REL_THRESHOLD 0.0166 // default 0.166
#define EDGE_SEARCH_MAX_ITER 100

uniform int DEBUG_LAYER;

uniform vec2 iResolution;

uniform sampler2D inputTexture;

in vec2 uv;

out vec4 fragColor;


float get_luma(vec2 uv, vec2 offset){
    return texture(inputTexture, uv + offset).a;
}

void do_fxaa(inout vec3 color, inout int info){
    // compute pixel size
    float dx = 1 / iResolution.x;
    float dy = 1 / iResolution.y;

    // compute luma data
    float lumaC = get_luma(uv, vec2(0, 0));
    float lumaN = get_luma(uv, vec2(0, dy));
    float lumaS = get_luma(uv, vec2(0, -dy));
    float lumaE = get_luma(uv, vec2(dx, 0));
    float lumaW = get_luma(uv, vec2(-dx, 0));
    float lumaNE = get_luma(uv, vec2(dx, dy));
    float lumaSE = get_luma(uv, vec2(dx, -dy));
    float lumaNW = get_luma(uv, vec2(-dx, dy));
    float lumaSW = get_luma(uv, vec2(-dx, -dy));

    float lumaMax = max(max(max(max(lumaN, lumaS), lumaE), lumaW), lumaC);
    float lumaMin = min(min(min(min(lumaN, lumaS), lumaE), lumaW), lumaC);
    float range = lumaMax - lumaMin;

    // skip unnecessary pixels
    if (range < max(LUMA_ABS_THRESHOLD, LUMA_REL_THRESHOLD*lumaMax)){
        range = 0.0;
        return;
    }

    // subpixel blend factor
    float averageLum = 2.0 * (lumaN + lumaS + lumaE + lumaW);
    averageLum += lumaNE + lumaSE + lumaNW + lumaSW;
    averageLum = averageLum / 12.0;
    float subpixelFilterStrength = abs(averageLum - lumaC) / range;
    subpixelFilterStrength = smoothstep(0, 1, clamp(subpixelFilterStrength, 0.0, 1.0));
    subpixelFilterStrength = subpixelFilterStrength * subpixelFilterStrength;

    // EDGE BLENDING —————————————————————————————————————————————

    // determine edge direction
    // we compute the luminance gradient
    float horizontal = 2.0*abs(lumaN + lumaS - 2.0*lumaC)
                          +abs(lumaNE + lumaSE - 2.0*lumaE)
                          +abs(lumaNW + lumaSW - 2.0*lumaW);
    float vertical = 2.0*abs(lumaE + lumaW - 2.0*lumaC)
                        +abs(lumaNE + lumaNW - 2.0*lumaN)
                        +abs(lumaSE + lumaSW - 2.0*lumaS);

    vec2 pixelOffset;
    vec2 searchStep;
    float lumaPos, lumaNeg;
    // is the edge vertical horizontal
    if (horizontal > vertical){
        pixelOffset = vec2(0, dy);
        searchStep = vec2(dx, 0);
        lumaPos = lumaN;
        lumaNeg = lumaS;
    } else {
        pixelOffset = vec2(dx, 0);
        searchStep = vec2(0, dy);
        lumaPos = lumaE;
        lumaNeg = lumaW;
    }

    // should we blend in the positive or negative direction
    // we compute the edge gradient
    float gradPos = abs(lumaPos - lumaC);
    float gradNeg = abs(lumaNeg - lumaC);
    float lumaGrad, otherLuma;
    if (gradPos < gradNeg){
        pixelOffset = -pixelOffset;
        lumaGrad = gradNeg;
        otherLuma = lumaNeg;
    } else {
        lumaGrad = gradPos;
        otherLuma = lumaPos;
    }

    // edge blend factor
    vec2 edgeSearchStart = uv + pixelOffset*0.5;
    float edgeLuma = 0.5*(lumaC + otherLuma);
    float gradientThreshold = 0.25*lumaGrad;
    bool edgeSign = (lumaC - edgeLuma) >= 0;

    vec2 searchOffsetP = vec2(0, 0);
    float lumaDeltaP;
    for (int s = 0; s <= EDGE_SEARCH_MAX_ITER; s++){
        searchOffsetP += searchStep;
        float l = get_luma(edgeSearchStart, searchOffsetP);
        lumaDeltaP = l - edgeLuma;
        if (abs(lumaDeltaP) > gradientThreshold){
            break; // we have found the end of the edge
        }
    }

    vec2 searchOffsetN = vec2(0, 0);
    float lumaDeltaN;
    for (int s = 0; s <= EDGE_SEARCH_MAX_ITER; s++){
        searchOffsetN -= searchStep;
        float l = get_luma(edgeSearchStart, searchOffsetN);
        lumaDeltaN = l - edgeLuma;
        if (abs(lumaDeltaN) > gradientThreshold){
            break; // we have found the end of the edge
        }
    }

    searchOffsetP = abs(searchOffsetP);
    searchOffsetN = abs(searchOffsetN);
    float distToP = searchOffsetP.x + searchOffsetP.y;
    float distToN = searchOffsetN.x + searchOffsetN.y;
    float distToNearestEnd;
    bool deltaSign;
    if (distToP < distToN){
        distToNearestEnd = distToP;
        deltaSign = lumaDeltaP >= 0;
    } else {
        distToNearestEnd = distToN;
        deltaSign = lumaDeltaN >= 0;
    }
    if (deltaSign == edgeSign){
        return; 
    }

    float edgeFilterStrength = 0.5 - distToNearestEnd / (distToP + distToN);


    // FINAL RESULT —————————————————————————————————————————

    // combine the two filters
    float filterStrength = max(subpixelFilterStrength, edgeFilterStrength);

    // blend
    if (DEBUG_LAYER == 5){
        info = 1;
        color = vec3(filterStrength, 0, 0);
        return;
    }
    color = texture(inputTexture, uv + pixelOffset*filterStrength).rgb;

}

void main(){

    vec4 inColor = texture(inputTexture, uv);
    vec3 color = inColor.rgb;
    int info = 0;

    // apply filter
    if (DEBUG_LAYER == 7){
        if (info == 0){
            color = vec3(0);
        }
        /* if (info == 1){ */
        /*     color = vec3(0, 1, 0); */
        /* } */
        /* if (info == 2){ */
        /*     color = vec3(1, 0, 0); */
        /* } */
    }
    if (DEBUG_LAYER == 6){
    } else {
        do_fxaa(color, info);
        if (DEBUG_LAYER == 5){
            if (info == 0){
                color = vec3(inColor.a);
            }
        }
    }

    fragColor = vec4(color, 1);

}
