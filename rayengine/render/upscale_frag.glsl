
#version 430

uniform float upscale_factor;
uniform vec2 iResolution;
uniform sampler2D mytexture;

in vec2 uv;

out vec4 fragColor;

#define BLACK vec3(0, 0, 0)
#define WHITE vec3(1, 1, 1)

void main(){

    fragColor = vec4(BLACK, 1.0);

    vec4 a = texture(mytexture, uv);

    fragColor.rgb = a.rgb;

}
