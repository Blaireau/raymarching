
float ray_intersect_sphere(vec3 rayStart, vec3 direction, vec3 center, float radius){
    vec3 relStart = rayStart - center;
    float b = dot( relStart, direction );
    float c = dot( relStart, relStart ) - radius*radius;
    if (c < 0){
        return EPSILON;
    }
    float h = b*b - c;
    if( h < 0.0 ){
        return -1.0; // no intersection
    }
    h = sqrt(h);
    /* return vec2( -b-h, -b+h ); */
    return -b-h;
}

vec3 rotate(vec3 vec, vec3 axis, float angle){
    vec3 k = axis*dot(axis, vec);
    vec3 l = cross(axis, vec);
    return (vec-k)*cos(angle) + l*sin(angle) + k;
}
