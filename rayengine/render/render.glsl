Model read_model(int modelIndex){
    ModelData data = modlist[modelIndex];
    return Model(data.posData.xyz, data.posData.w, data.orientData,
                 data.indexData.x, data.indexData.y);
}

Light read_light(int lightIndex){
    LightData data = lightlist[lightIndex];
    return Light(data.colorData.rgb, data.orientData.xyz, data.indexData.x);
}

bool ray_intersect_bound(vec3 rayPos, vec3 direction, Model model, float offset_factor){
    switch (model.boundType){
        case (SPHERE_BOUND_TYPE):
            vec3 towardObject = model.pos - rayPos;
            float dist = dot(towardObject, direction);
            if (-model.boundValue < dist && dist < MAX_RENDER_DIST){
                float offset = dist*offset_factor;
                float perpDist = length(towardObject - dist*direction);
                if (perpDist < model.boundValue + offset){
                    return true;
                }
            }
            return false;
        case (PLANE_BOUND_TYPE):
            if (rayPos.z < (model.boundValue+offset_factor)){ // we are below the plane
                return true; 
            }
            if (direction.z > 0){ // we are above and going up
                return false; 
            }
            return true;
    }
    return true; // still intersect if no bound is defined
} 

int get_intersecting_models(vec3 rayPos, vec3 direction, float offset_factor,
                            inout int[MAX_MODELS_ON_RAY] modelsOnRay){
    int modelCount = 0;
    for (int index = 0; index < modlist.length(); index++){
        Model model = read_model(index);
        if (ray_intersect_bound(rayPos, direction, model, offset_factor)){
            modelsOnRay[modelCount] = index;
            modelCount++;
            if (modelCount > MAX_MODELS_ON_RAY){
                return modelCount;
            }
        }
    }
    return modelCount;
}

Ray build_ray(vec3 rayOrigin, vec3 rayDirection, float offset_factor){
    int [MAX_MODELS_ON_RAY] modelsOnRay;
    int modelCount = get_intersecting_models(rayOrigin, rayDirection, offset_factor, modelsOnRay);
    Ray ray = Ray(rayOrigin, rayDirection, modelsOnRay, modelCount);
    return ray;
}

float sminExp(float a, float b, float k){
    float res = exp2(-k*a) + exp2(-k*b);
    return -log2(res)/k;
}

PointOnRay globalSDF(vec3 point, float dist, inout Ray ray){
    PointOnRay bestPoint = PointOnRay(0, MAX_RENDER_DIST, vec3(0));
    for (int index = 0; index < ray.modelCount; index++){ 
        int modelIndex = ray.modelsOnRay[index];
        Model model = read_model(modelIndex);
        PointOnRay newPoint = getSDF(point - model.pos, model.sdfID, dist, ray);
        bestPoint = Union(newPoint, bestPoint);
    }
    return bestPoint;
}

vec3 get_normal(vec3 point, float dist, inout Ray ray){
    float dx = (  globalSDF(point + UNIT_X * NORMALS_EPSILON, -1, ray).dist
                - globalSDF(point - UNIT_X * NORMALS_EPSILON, -1, ray).dist);
    float dy = (  globalSDF(point + UNIT_Y * NORMALS_EPSILON, -1, ray).dist
                - globalSDF(point - UNIT_Y * NORMALS_EPSILON, -1, ray).dist);
    float dz = (  globalSDF(point + UNIT_Z * NORMALS_EPSILON, -1, ray).dist
                - globalSDF(point - UNIT_Z * NORMALS_EPSILON, -1, ray).dist);
    return normalize(vec3(dx, dy, dz));
}

vec3 get_ray_direction(vec2 screenPos){
    vec3 rayDir = screenPos.x*cameraRight + screenPos.y*cameraUp + SCREEN_DISTANCE*cameraDir;
    return(normalize(rayDir));
}

float get_shadow(vec3 rayOrigin, vec3 direction, float startDist, out vec3 shadowdebug){ 
    Ray lightray = build_ray(rayOrigin, direction, SOFT_SHADOW_RADIUS_INC);
    float dist = startDist;
    float shadow = 1.0;

    for (int i = 0; i < SHADOW_MAX_ITER && dist < SHADOW_MAX_RENDER_DIST; i++){
        vec3 rayPos = lightray.origin + dist*lightray.direction;
        float dist_inc = globalSDF(rayPos, -1, lightray).dist;
        if (dist_inc < EPSILON*dist){
            shadowdebug = RED;
            return 0.0; // obstruction, we get no light
        }
        shadow = min(shadow, SOFT_SHADOW_FACTOR*dist_inc/dist);
        dist += dist_inc;
    }
    shadowdebug = ORANGE;
    return shadow; // soft shadow
}

vec3 do_material_lightning(vec3 viewDir, PointOnRay renderPoint,
                           out float shadow,
                           out vec3 shadowdebug,
                           inout Ray ray){
    vec3 pos = ray.origin + renderPoint.dist*ray.direction; 
    vec3 lum = BLACK;
    vec3 normal = get_normal(pos, renderPoint.dist, ray);
    Material material = getMaterial(renderPoint.localPoint, normal, renderPoint.materialID);
    for (int l = 0; l < lightCount; l++){
        Light light = read_light(l);
        shadow = 1.0;
        if (light.isKeyLight == 1){
            shadow = get_shadow(pos, light.direction, SHADOW_START_DIST, shadowdebug); 
            vec3 reflected = reflect(light.direction, normal);
            float specularFactor = clamp(dot(reflected, viewDir), 0.0, 1.0);
            lum += pow(specularFactor, material.metalic)*light.color*shadow;
        }
        float diffuseFactor = clamp(dot(normal, light.direction), 0.0, 1.0); 
        lum += diffuseFactor*light.color*shadow;
    }
    return material.color*lum;
}

void cast_ray(out PointOnRay renderPoint, out int iter, out float smallestDistInc,
              inout Ray ray){
    iter = 0;
    renderPoint = PointOnRay(0, 0.0, vec3(0));

    vec3 rayPos = ray.origin;

    bool bisectionMode = false;
    float lowDist = 0;
    float highDist = 0;
    for (int i = 1; i <= MAX_ITER; i++){
        iter = i;
        if (bisectionMode){
            /* return; */
            renderPoint.dist = (highDist + lowDist) / 2.0;
            rayPos = ray.origin + renderPoint.dist*ray.direction;
            PointOnRay res = globalSDF(rayPos, -1, ray);
            renderPoint.materialID = res.materialID;
            renderPoint.localPoint = res.localPoint;
            if (res.dist > 0){
                lowDist = renderPoint.dist;
            } else {
                highDist = renderPoint.dist;
            }
            if ((highDist - lowDist) < EPSILON*renderPoint.dist){
                return;
            }
        } else {
            // normal marching mode
            PointOnRay res = globalSDF(rayPos, renderPoint.dist, ray);
            float dist_inc = res.dist;
            smallestDistInc = min(dist_inc, smallestDistInc);
            lowDist = highDist;
            highDist = renderPoint.dist;
            renderPoint.dist += dist_inc;
            renderPoint.localPoint = res.localPoint;
            renderPoint.materialID = res.materialID;
            rayPos = ray.origin + renderPoint.dist*ray.direction;
            if (dist_inc < EPSILON*renderPoint.dist){
                bisectionMode = true;
            } else if (renderPoint.dist > MAX_RENDER_DIST){
                // we are too far, we hit the sky
                renderPoint.materialID = 0;
                return;
            }
        }
    }
    // we stop eventhough we were not yet close enough
    // but we have to return at some point
    renderPoint.dist = MAX_RENDER_DIST;
    renderPoint.materialID = 0;
    return;
}

float remap(float a, float b, float x){
    return clamp(0, 1, (x - a) / (b - a));
}

float random_sphere(vec3 pos, vec3 cell){
    float starAlt = 200.0;
    float starAmp = 10.0;
    float noise = mynoise(cell.z+cell.x, cell.y + cell.x);
    if (abs(length(cell) - starAlt) - starAmp > 0 || noise < 0.995){
        return 10000.0;
    }
    float radnoise = mynoise(cell.x, cell.y);
    return length(cell - pos) - 0.2*mix(0.6, 1.0, radnoise);
}

float sphere_noise(vec3 point){
    vec3 cell = floor(point);
    float dist = min(min(min(min(min(min(min(
                       random_sphere(point, cell + vec3(0, 0, 0)),
                       random_sphere(point, cell + vec3(0, 0, 1))),
                       random_sphere(point, cell + vec3(0, 1, 0))),
                       random_sphere(point, cell + vec3(0, 1, 1))),
                       random_sphere(point, cell + vec3(1, 0, 0))),
                       random_sphere(point, cell + vec3(1, 0, 1))),
                       random_sphere(point, cell + vec3(1, 1, 0))),
                       random_sphere(point, cell + vec3(1, 1, 1)));
    return dist;
}

vec3 draw_stars(vec3 direction, vec3 color){
    vec3 point = vec3(0);
    float dist = 190.0;
    float min_inc = 1000.0;
    mat3 Ma = mat3(4.0/5.0, 3.0/5.0, 0.0,
                  -3.0/5.0, 4.0/5.0, 0.0,
                   0.0, 0.0, 1.0);
    mat3 Mb = mat3(1.0, 0.0, 0.0,
                   0.0, 4.0/5.0, 3.0/5.0,
                   0.0, -3.0/5.0, 4.0/5.0);
    float alpha = iTime*PI/12.0; // one turn in 24 hours
    mat3 starRot = mat3(cos(alpha), 0, sin(alpha),
                                 0, 1,          0,
                       -sin(alpha), 0, cos(alpha));
    direction = starRot*direction;
    for (int i = 0; i < 40; i++){
        point = direction*dist;
        float dist_inc = 200*min(min(sphere_noise(point),
                                     sphere_noise(Ma*point)),
                                     sphere_noise(Mb*point));
        min_inc = min(min_inc, dist_inc);
        dist += 10.0;
        if (dist_inc < 0.0){
            break;
        }
    }
    return mix(WHITE, color, smoothstep(-0.1, 10, min_inc));
}

float cloudsdf(vec3 point, float cloudAlt, float cloudWidth){
    point = (point - vec3(2502*iTime, 4364*iTime, cloudAlt))/cloudWidth;
    float density = (0.5+0.5*montain_noise(point.xy, 6))-abs(point.z - 0.5); 
    float cloudfilter = max(0.5, 1.0+(iTime-6.0)*(iTime-22.0)/32.0);
    return max(cloudWidth*(density-cloudfilter), 0.0);
}

/* vec3 cloudgrad(vec3 point, float cloudAlt, float cloudWidth){ */
/*     point = (point - vec3(0, 0, cloudAlt))/cloudWidth; */
/*     /1* vec3 grad; *1/ */
/*     /1* grad.xy = 0.5*montain_grad(point.xy, 2); *1/ */
/*     /1* grad.z = -sign(point.z - 0.5); *1/ */
/*     float dx = (  cloudsdf(point + UNIT_X * NORMALS_EPSILON, cloudAlt, cloudWidth) */
/*                 - cloudsdf(point - UNIT_X * NORMALS_EPSILON, cloudAlt, cloudWidth)); */
/*     float dy = (  cloudsdf(point + UNIT_Y * NORMALS_EPSILON, cloudAlt, cloudWidth) */
/*                 - cloudsdf(point - UNIT_Y * NORMALS_EPSILON, cloudAlt, cloudWidth)); */
/*     float dz = (  cloudsdf(point + UNIT_Z * NORMALS_EPSILON, cloudAlt, cloudWidth) */
/*                 - cloudsdf(point - UNIT_Z * NORMALS_EPSILON, cloudAlt, cloudWidth)); */
/*     return normalize(vec3(dx, dy, dz)); */
/*     /1* return normalize(cloudWidth*grad); *1/ */
/* } */

vec3 do_sky(Ray ray, inout vec3 level){
        Light sun = read_light(0);
        float sunAlt = 100000;
        float sunRad = 3000;
        float ca = dot(sun.direction, ray.direction);
        float sa = length(cross(sun.direction, ray.direction));
        float sundist = sunAlt*sa/ca-sunRad;
        float skyfactor;
        if (ca > 0){
            skyfactor = 1.0 - clamp(0, 1, exp(-4*sundist/sunAlt) + pow(1 - ray.direction.z, 12));
        } else {
            skyfactor = 1.0 - pow(1 - ray.direction.z, 12);
            sundist = sunAlt;
        }
        vec3 color = mix(skylite, skydark, clamp(0, 1, skyfactor));
        // draw a nice white circle for the sun
        color = mix(WHITE, color, smoothstep(0, 500.0, sundist));
        // draw some stars at night
        float luma = 0.2126*color.r + 0.7152*color.g + 0.0722*color.b;
        float starLumaMax = 0.25;
        if (luma < starLumaMax){
            vec3 starcolor = draw_stars(ray.direction, color);
            color = mix(starcolor, color, smoothstep(0.0, starLumaMax, luma));
        }
        /* return color; */
        if (ray.direction.z < 0){
            return color;
        }
        // clouds
        float steps = 60;
        float cloudAlt = 2000.0;
        float cloudWidth = 1000.0;
        float stepSize = cloudWidth / (ray.direction.z * steps);
        stepSize = min(stepSize, cloudWidth*0.1);
        float maxAlt = cloudAlt + cloudWidth;
        float dist = (cloudAlt - ray.origin.z) / ray.direction.z;
        vec3 point = ray.origin;
        float transmittance = 1.0;
        float lightenergy = -1.0;
        float absorptionFactor = 0.000004;

        vec3 firstPoint;
        for (int i = 0; i < steps; i++){
            point = ray.origin + ray.direction*dist;
            float d_inc = cloudsdf(point, cloudAlt, cloudWidth);
            if (d_inc > 0.0){
                if (lightenergy < 0.0){
                    firstPoint = point;
                }
                transmittance *= exp(-d_inc*stepSize*absorptionFactor);
            }
            if (transmittance < 0.001){
                break;
            }
            dist += stepSize;
        }
        vec3 midPoint = (point + firstPoint) / 2.0;
        float density = cloudsdf(midPoint, cloudAlt, cloudWidth);
        lightenergy = 0.6 + 0.4*exp(-density*cloudWidth*absorptionFactor);
        vec3 cloudbasecolor = mix((skylite+skydark)/2.0, WHITE, 0.05);
        vec3 cloudcolor = mix(BLACK, cloudbasecolor, lightenergy);
        color = mix(cloudcolor, color, transmittance); 
        level = mix(BLACK, WHITE, transmittance);
        return color;
}

void main(){

    vec2 screen_pos = (2*uv-1);
    screen_pos.x = screen_pos.x * iResolution.x / iResolution.y;

    vec3 color = BLACK;


    // get the ray direction 
    vec3 rayVector = get_ray_direction(screen_pos);
    // build the ray
    Ray ray = build_ray(cameraPos, rayVector, 0.0);
    // cast the marching ray
    int iter;
    PointOnRay renderPoint;

    float smallestDistInc = EPSILON; //TODO: REMOVE IF UNUSED
    cast_ray(renderPoint, iter, smallestDistInc, ray);

    float shadow = 1.0;
    vec3 shadowdebug = BLACK;
    vec3 level = BLACK;
    if (renderPoint.dist < MAX_RENDER_DIST){
        color = do_material_lightning(cameraDir, renderPoint, shadow, shadowdebug, ray);
        // basic fog (TEMP)
        vec3 lambda = exp(-0.00003*renderPoint.dist*vec3(1, 2, 4));
        float fogbase = clamp(0.01, 0.3, length(skydark));
        color = mix(vec3(fogbase), color, lambda);
        // end fog
    } else {
        // some sky testing
        color = do_sky(ray, level);
    }

    // DEBUG STUFF
    if (DEBUG_LAYER == 1){
        // distance to object
        if (renderPoint.dist < 0){
            color = RED;
        } else {
            color = mix(GREEN, BLACK, clamp(renderPoint.dist/MAX_RENDER_DIST, 0.0, 1.0));
        }
    }
    if (DEBUG_LAYER == 2){
        // iterations
        color = mix(BLACK, RED, iter/float(MAX_ITER));
        color = BLACK;
        if (iter > ITER_XLOW){
            color.b += 0.2;
        }
        if (iter > ITER_LOW){
            color.b += 0.2;
        }
        if (iter > ITER_MEDIUM){
            color.b += 0.2;
        }
        if (iter > ITER_HIGH){
            color.b += 0.2;
        }
        if (iter > ITER_GHIGH){
            color.b += 0.2;
        }
        if (iter >= MAX_ITER){
            color.rgb = vec3(1);
        }
    }
    if (DEBUG_LAYER == 3){
        // model count
        color = mix(BLACK, WHITE, ray.modelCount / 3.0);
    }
    if (DEBUG_LAYER == 4){
        // shadows map
        color = level;
    }


    // gamma correction
    color = pow(color, vec3(1.0/2.2));

    // compute luminance
    // https://en.wikipedia.org/wiki/Luma_(video)
    float luma = 0.2126*color.r + 0.7152*color.g + 0.0722*color.b;
    luma = pow(luma, (1.0/3.0));

    fragColor = vec4(color, luma);
    /* fragColor = vec4(color, color.r); */
}
