from arcade.experimental.texture_render_target import RenderTargetTexture

import struct
from pathlib import Path



BASE_DIR = Path(__file__).parent

MODEL_BUFFER_FORMAT = 'ffff' \
                      'ffff' \
                      'iiii'
LIGHTS_BUFFER_FORMAT = 'ffff' \
                       'ffff' \
                       'iiii'

RENDER_TEXTURE = 0
AA_TEXTURE = 1
UPSCALE_TEXTURE = 2


class Shader(RenderTargetTexture):

    def __init__(self, width, height, fragment, texture):
        super().__init__(width, height)

        vertex = self.load_shader_source('vertex_shader.glsl')

        self.program = self.ctx.program(
            vertex_shader=vertex,
            fragment_shader=fragment,
        )
        self.destTextureNumber = texture

    def set_debug_mode(self, value):
        self.set_uniform('DEBUG_LAYER', value)

    def load_shader_source(self, source):
        with open(BASE_DIR/source) as file:
            content = file.read()
        return content

    def set_uniform(self, key, val):
        try:
            self.program[key] = val
        except KeyError:
            print(f'no uniform : {key}')
            pass

    def use(self):
        self._fbo.use()

    def draw(self):
        self.texture.use(self.destTextureNumber)
        self._quad_fs.render(self.program)


class RenderShader(Shader):

    def __init__(self, width, height, usermodels):
        header = self.load_shader_source('header.glsl')
        mathtools = self.load_shader_source('mathtools.glsl')
        primitives = self.load_shader_source('primitives.glsl')
        render = self.load_shader_source('render.glsl')
        fragment = '\n'.join([header, mathtools, primitives, usermodels, render])

        super().__init__(width, height, fragment, RENDER_TEXTURE)
        self.set_uniform('iTime', 0.0)
        self.set_uniform('iResolution', (width, height))
        self.set_uniform('testParam', 0.0)

    def update_models(self, models):
        flat_array = [v for mod in models for v in mod]
        buffer_format = MODEL_BUFFER_FORMAT * len(models)
        ar = struct.pack(buffer_format, *flat_array)
        self.models_ssbo = self.ctx.buffer(data=ar)

    def update_lights(self, lightsData):
        flat_array = [v for light in lightsData for v in light]
        buffer_format = LIGHTS_BUFFER_FORMAT * len(lightsData)
        self.set_uniform('lightCount', len(lightsData))
        ar = struct.pack(buffer_format, *flat_array)
        self.lights_ssbo = self.ctx.buffer(data=ar)

    def update_camera(self, cameraData):
        for key, val in cameraData.items():
            self.set_uniform(key, val)

    def draw(self):
        self.models_ssbo.bind_to_storage_buffer(binding=0)
        self.lights_ssbo.bind_to_storage_buffer(binding=1)
        super().draw()


class AntiAliasingShader(Shader):

    def __init__(self, width, height):
        fragment = self.load_shader_source('fxaa.glsl')
        super().__init__(width, height, fragment, AA_TEXTURE)
        self.set_uniform('iResolution', (width, height))
        self.set_uniform('mytexture', RENDER_TEXTURE)


class UpscaleShader(Shader):
    def __init__(self, width, height, upscale_factor):

        upscale_frag = self.load_shader_source('upscale_frag.glsl')
        super().__init__(width, height, upscale_frag, UPSCALE_TEXTURE)
        self.set_uniform('iResolution', (width, height))
        self.set_uniform('mytexture', AA_TEXTURE)
        self.set_uniform('upscale_factor', upscale_factor)
