import time


MAX_DT_VEC_SIZE = 20


class PerfMeter:

    def __init__(self, output_template, max_size=MAX_DT_VEC_SIZE):
        self.output_template = output_template
        self._dtvec = []
        self._max_size = max_size
        self.t0 = None

    def _push_dt(self, dt):
        self._dtvec.append(dt)
        self.last_dt = dt
        if len(self._dtvec) > self._max_size:
            self._dtvec.pop(0)

    def push_dt(self, dt):
        self._push_dt(dt)

    @property
    def fps(self):
        return 1 / self.value

    @property
    def value(self):
        return sum(self._dtvec) / len(self._dtvec)

    def __str__(self):
        return self.output_template.format(fps=round(self.fps),
                                           value=self.value,
                                           value_ms=round(self.value*1000))
    def __repr__(self):
        return str(self)

    def start_stopwatch(self):
        self.t0 = time.time()

    def stop_stopwatch(self):
        if self.t0 is None:
            raise Exception('You have to start the stopwatch before stopping it')
        dt = time.time() - self.t0
        self._push_dt(dt)
        self.t0 = None
