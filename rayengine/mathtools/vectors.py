from math import sqrt

EPSILON = 1e-6


class Vec3:

    def __init__(self, a=None, b=None, c=None):
        if b is None and c is None:
            if isinstance(a, Vec3):
                self._values = list(a._values)
            elif a is None:
                self._values = [0, 0, 0]
            else:
                raise ValueError(f'Vec should be initialized with another Vec3'
                                 f' or 3 numbers, not {a}')
        elif b is not None and c is not None:
            self._values = [a, b, c]
        else:
            raise ValueError(f'Vec should be initialized with another Vec3 or 3 numbers')

    def __eq__(self, vec):
        return (self - vec).norm() < EPSILON

    def __iter__(self):
        return iter(self._values)

    @property
    def x(self):
        return self._values[0]

    @property
    def y(self):
        return self._values[1]

    @property
    def z(self):
        return self._values[2]

    def get_val(self):
        return list(self)

    def __add__(self, vec):
        return Vec3(*[u+v for u, v in zip(self._values, vec._values)])

    def __mul__(self, scalar):
        return Vec3(*[u*scalar for u in self._values])

    def __truediv__(self, scalar):
        return Vec3(*[u/scalar for u in self._values])

    def __iadd__(self, vec):
        return self + vec

    def __neg__(self):
        return Vec3(*(self*(-1))._values)

    def norm(self):
        return sqrt(sum([u**2 for u in self._values]))

    def __sub__(self, vec):
        return Vec3(*(self + (vec*(-1)))._values)

    def __repr__(self):
        vecData = '; '.join(f'{round(u, 2)}' for u in self._values)
        return f"({vecData})"

    def unit(self):
        norm = self.norm()
        if norm == 0:
            return Vec3(self)
        return Vec3(*[u / norm for u in self._values])

    def cross(self, vec):
        return Vec3(*[self.y*vec.z - self.z*vec.y,
                      self.z*vec.x - self.x*vec.z,
                      self.x*vec.y - self.y*vec.x])

    def dot(self, vec):
        return sum([u*v for u, v in zip(self._values, vec._values)])

    def rotated(self, rotation):
        return rotation.rotate_vector(self)
