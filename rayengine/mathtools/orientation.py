from math import cos, sin

from rayengine.mathtools.vectors import Vec3, EPSILON
from rayengine.mathtools.quaternions import Quaternion



class Orientation:

    def __init__(self, otherObject=None, angle=0):
        self._angle = None
        self._axis = None
        if otherObject is None:
            self._angle = 0
            self._axis = Vec3(0, 0, 1)
            self.compute_quaternion()
        elif isinstance(otherObject, Orientation):
            self._quat = Quaternion(otherObject._quat)
        elif isinstance(otherObject, Quaternion):
            self._quat = Quaternion(otherObject)
        elif isinstance(otherObject, Vec3):
            self._angle = angle
            self._axis = Vec3(otherObject).unit()
            self.compute_quaternion()
        else:
            raise ValueError('Orientation should be initialized with '
                             'a Quaternion, another Orientation or a Vec3 and an angle')

    def __eq__(self, other):
        return self._quat == other._quat

    @property
    def angle(self):
        self.build_axis_angle_representation()
        return self._angle

    @angle.setter
    def angle(self, value):
        self.build_axis_angle_representation()
        self._angle = value
        self.compute_quaternion()

    def build_axis_angle_representation(self):
        if self._axis is None:
            self._axis, self._angle = self._quat.as_axis_angle()

    def __and__(self, other):
        total_rot = other._quat * self._quat
        return Orientation(total_rot)

    def compute_quaternion(self):
        thetaHalf = self._angle / 2
        realPart = Quaternion(cos(thetaHalf), 0, 0, 0)
        vectPart = Quaternion(0, *list(self._axis))*sin(thetaHalf)
        self._quat = realPart + vectPart

    def rotate_vector(self, vector):
        p = Quaternion(0, *list(vector))
        q1 = self._quat
        q2 = self._quat.conjugate()
        rotp = q1*p*q2
        return Vec3(rotp.x, rotp.y, rotp.z)

    def __repr__(self):
        self.build_axis_angle_representation()
        return f'Orientation: axis = {self._axis} angle = {self._angle}'

