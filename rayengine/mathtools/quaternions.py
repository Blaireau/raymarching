from math import atan2, sqrt

from rayengine.mathtools import Vec3, EPSILON


def real_to_quaternion(number):
    return Quaternion(number, 0, 0, 0)

class Quaternion:

    def __init__(self, r, x=None, y=None, z=None):
        if isinstance(r, Quaternion) and x is None and y is None and z is None:
            self.r = r.r
            self.x = r.x
            self.y = r.y
            self.z = r.z
        elif not(x is None or y is None or z is None):
            self.r = r
            self.x = x
            self.y = y
            self.z = z
        else:
            raise ValueError('Quaternion should be initialized with another quaternion'
                             ' or 4 numbers.')

    def __eq__(self, other):
        if isinstance(other, (int, float)):
            other = real_to_quaternion(other)
        return (self - other).norm() < EPSILON

    def __add__(self, other):
        if isinstance(other, (int, float)):
            other = real_to_quaternion(other)
        return Quaternion(self.r + other.r,
                          self.x + other.x,
                          self.y + other.y,
                          self.z + other.z)

    def __sub__(self, other):
        return self + (-other)

    def __repr__(self):
        return f'Quaternion : {self.r} + {self.x} i + {self.y} j + {self.z} k'

    def __neg__(self):
        return self*(-1)

    def __pow__(self, number):
        if not (isinstance(number, int) and number > 0):
            raise ValueError(f'Quaternion exponent should be a positive integer, not {number}')
        res = Quaternion(self)
        for _ in range(number-1):
            res = res*self
        return res

    def __mul__(self, other):
        if not isinstance(other, Quaternion):
            if isinstance(other, (int, float)):
                other = real_to_quaternion(other)
            else:
                raise ValueError(f'Multiplication between Quaternion and {other} is not defined')
        nr = self.r*other.r - self.x*other.x - self.y*other.y - self.z*other.z
        nx = self.x*other.r + self.r*other.x + self.y*other.z - self.z*other.y
        ny = self.y*other.r + self.r*other.y + self.z*other.x - self.x*other.z
        nz = self.z*other.r + self.r*other.z + self.x*other.y - self.y*other.x
        return Quaternion(nr, nx, ny, nz)

    def conjugate(self):
        return Quaternion(self.r, -self.x, -self.y, -self.z)

    def norm(self):
        return sqrt(self.r**2 + self.x**2 + self.y**2 + self.z**2)

    def as_axis_angle(self):
        axis = Vec3(self.x, self.y, self.z)
        angle = 2*atan2(axis.norm(), self.r)
        return axis.unit(), angle
