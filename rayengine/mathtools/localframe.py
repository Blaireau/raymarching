from math import sqrt, acos

from rayengine.mathtools import Vec3
from rayengine.mathtools import Orientation

ABSOLUTE_AXIS = [
    Vec3(1, 0, 0),
    Vec3(0, 1, 0),
    Vec3(0, 0, 1)
]

class LocalFrame:

    def __init__(self, pos=None):
        self._origin = Vec3(pos)
        self._speed = Vec3(0, 0, 0)
        self._orientation = Orientation()
        self._rotation = Orientation()
        self._angular_speed = 0
        self.update(0)

    @property
    def origin(self):
        return self._origin

    @origin.setter
    def origin(self, value):
        self._origin = Vec3(value)
        self.update()

    @property
    def orientation(self):
        return self._total_orientation

    @orientation.setter
    def orientation(self, value):
        if not isinstance(value, Orientation):
            raise ValueError(f'Invalid orientation {value}')
        self._orientation = value
        # if we set explicitely the orientation, we reset the rotation
        self._rotation = Orientation()
        self._angular_speed = 0
        self.update()

    @property
    def speed(self):
        return self._speed

    @speed.setter
    def speed(self, value):
        self._speed = Vec3(value)
        self.update()

    @property
    def angular_speed(self):
        return self._angular_speed

    @angular_speed.setter
    def angular_speed(self, value):
        self._angular_speed = value
        self.update()

    @property
    def rotation(self):
        return self._rotation

    @rotation.setter
    def rotation(self, value):
        if not isinstance(value, Orientation):
            raise ValueError(f'Invalid rotation {value}')
        self._orientation = self._total_orientation
        self._rotation = value
        self.update()

    # def get_total_orientation(self):
    #     a = self.xaxis.x
    #     b = self.xaxis.y
    #     d = self.yaxis.x
    #     e = self.yaxis.y
    #     j = self.zaxis.z
    #     cosTheta = (a + e - sqrt((a - e)**2 + (b + d)**2)) / 2
    #     if cosTheta == 1:
    #         # there is no rotation (axis not rotated with respect to parent)
    #         # we can return any arbitrary rotation
    #         return Orientation()
    #     kx = sqrt((a - cosTheta) / (1 - cosTheta))
    #     ky = sqrt((e - cosTheta) / (1 - cosTheta))
    #     kz = sqrt((j - cosTheta) / (1 - cosTheta))
    #     angle = acos(cosTheta)
    #     return Orientation(kx, ky, kz, angle)

    def update(self, dt=0):
        self._origin += self._speed * dt
        self._rotation.angle += self._angular_speed * dt
        xaxis, yaxis, zaxis = [Vec3(axis) for axis in ABSOLUTE_AXIS]
        self._total_orientation = self._orientation & self._rotation
        self.xaxis = xaxis.rotated(self._total_orientation)
        self.yaxis = yaxis.rotated(self._total_orientation)
        self.zaxis = zaxis.rotated(self._total_orientation)

    def to_parent_coord(self, vec, orientationMode=False):
        localPos = self.xaxis*vec.x + self.yaxis*vec.y + self.zaxis*vec.z
        if orientationMode:
            return localPos
        return self._origin + localPos

    def __repr__(self):
        return f'LocalFrame origin: {self.origin} x: {self.xaxis} y: {self.yaxis} z: {self.zaxis}'
