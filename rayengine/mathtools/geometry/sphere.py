from dataclasses import dataclass

@dataclass
class Sphere:
    radius: int

    @property
    def data(self):
        return [self.radius, 0.0, 0.0, 0.0]

