from dataclasses import dataclass

from rayengine.tools import Vec3

@dataclass
class Plane:
    normal: Vec3

    @property
    def data(self):
        return [*self.normal.get_val(), 0.0]

