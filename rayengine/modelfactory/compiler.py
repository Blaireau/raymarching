import argparse
import os
import sys
from pathlib import Path

from rayengine.systools.filesystem import get_directory_path, get_files_with_ext, clean_directory
from rayengine.systools.default import *

from rayengine.parser.sourceparser import SourceParser
from rayengine.parser.errormanager import ErrorManager, ParseError
from rayengine.parser.astbuilder import ASTBuilder
from rayengine.parser.interpreter import Interpreter
from rayengine.modelfactory.builder import MaterialBuilder, ModelBuilder, MaterialRef


def read_file(filepath):
    with open(filepath, 'r') as file:
        return file.read()

def compile_file(objectType, builderClass, sourcePath, destinationPath, defineNames):
    print(f'Compiling "{sourcePath}" into "{destinationPath}"…', end=' ')
    source = read_file(sourcePath)
    header = f'Error in {objectType} file "{sourcePath}"'
    errorMgr = ErrorManager(header, source)
    parser = SourceParser(source, errorMgr)
    astbuilder = ASTBuilder(parser.tokens, errorMgr)

    interpreter = Interpreter(astbuilder.tree, errorMgr)
    if defineNames:
        for name in defineNames:
            interpreter.define_variable(name, MaterialRef(name))
    builder = builderClass(interpreter)
    try:
        builder.run()
        builder.save_result(destinationPath)
    except ParseError as message:
        print(message)
        sys.exit(1)
    print('Done.')

def change_suffix(filepath, suffix):
    return filepath.stem + suffix

def main(args):
    materialNames = build_all_files('material', MaterialBuilder)
    build_all_files('model', ModelBuilder, defineNames=materialNames.keys())

def build_all_files(objectType, builderClass, defineNames=None):
    sourceInfo = SOURCE_FILES[objectType]
    destInfo = COMPILED_FILES[objectType]
    destinationDir = get_directory_path(destInfo['directory'], make=True)
    clean_directory(destinationDir)
    # compile the models
    startDir = Path(sourceInfo['directory'])
    compiledNames = {}
    for file in startDir.iterdir():
        if file.suffix != sourceInfo['ext']:
            continue
        destinationPath = destinationDir / change_suffix(file, destInfo['ext'])
        compile_file(objectType, builderClass, file, destinationPath, defineNames)
        name = file.stem
        if name in compiledNames:
            print('Error: two files with the same name detected')
            print(file)
            print(compiledNames[name])
            sys.exit()
        else:
            compiledNames[file.stem] = file
    return compiledNames


if __name__ == '__main__':
    ap = argparse.ArgumentParser(description='Rayengine model compiler')
    # ap.add_argument('inputFiles',
    #                 metavar='file',
    #                 type=str,
    #                 help='A model file to compile or a folder of files',
    #                 nargs='+')
    # ap.add_argument('-d', '--outputDir',
    #                 metavar='directory',
    #                 type=str,
    #                 help='the directory for the compiled model file',
    #                 default=DEFAULT_COMPILE_MODEL_DIR,
    #                 required=False)
    args = ap.parse_args()
    main(args)



