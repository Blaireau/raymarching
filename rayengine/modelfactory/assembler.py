from rayengine.systools.filesystem import get_files_with_ext
from rayengine.systools.default import *

import json

DEBUG = True


MATERIAL_TEMPLATE = {
    'header': '''Material getMaterial(vec3 point, vec3 normal, int matID){
                     Material material;
                     switch (matID){''',
    'element': '''   case {n}:
                         material = get_material_{n}(point, normal);
                         break;''',
    'footer': '''    }
                     return material;
                 }''',
    'element-func': '''Material get_material_{n}(vec3 point, vec3 normal){{
                           return {body};
                       }}'''
}

MODEL_TEMPLATE = {
    'header': '''PointOnRay getSDF(vec3 point, int sdfID, float camDist, Ray ray){
                     PointOnRay renderPoint;
                     switch (sdfID){''',
    'element': '''   case {n}:
                         renderPoint = get_sdf_{n}(point, camDist, ray);
                         break;''',
    'footer': '''    }
                     return renderPoint;
                 }''',
    'element-func': '''PointOnRay get_sdf_{n}(vec3 point, float camDist, Ray ray){{
                           return {body};
                       }}'''
}

class Assembler:
    """
    Read all the compiled model files and assemble them into on global sdf function
    Output attributes:
        - outputSource : the glsl source code for the sdf function
        - modelDatabase : a dict of ModelData object
    """

    def __init__(self, template, replacePatterns=None):
        self.database = {}
        self.template = template
        self.replacePatterns = replacePatterns

    def assemble(self, files):
        # files = get_files_with_ext(modelDir, COMPILED_MODEL_FILE_EXT)
        generatedCode = []
        for index, file in enumerate(files):
            item = self.read_file(file)
            try:
                databaseElement = item['data']
            except KeyError:
                databaseElement = {}
            databaseElement['index'] = index
            self.database[file.stem] = databaseElement
            code = self.pack_as_function(index, item['code'])
            generatedCode.append(code)
        getFunctionCode = self.generate_get_function(len(files))
        generatedCode.append(getFunctionCode)
        self.outputSource = '\n'.join(generatedCode)
        # self.write_output_file(modelCodeFile, self.outputSource)
        if len(self.database) == 0:
            raise Exception('No models loaded')

    def read_file(self, filepath):
        with open(filepath, 'r') as file:
            data = json.load(file)
        return data

    def generate_get_function(self, count):
        lines = [self.template['header'],
                 *[self.template['element'].format(n=index) for index in range(count)],
                 self.template['footer']
                ]
        return '\n'.join(lines)

    def pack_as_function(self, index, body):
        newbody = body
        if self.replacePatterns:
            for pattern, replacement in self.replacePatterns.items():
                newbody = newbody.replace(pattern, replacement)
        return self.template['element-func'].format(n=index, body=newbody)


def assemble_models():
    # material stage
    matAssembler = Assembler(MATERIAL_TEMPLATE)
    materialFiles = get_files_with_ext(**COMPILED_FILES['material'])
    matAssembler.assemble(materialFiles)
    patterns = {f'#{name}#': str(data['index']) for name, data in matAssembler.database.items()}
    # model stage
    modelAssembler = Assembler(MODEL_TEMPLATE, patterns)
    modelFiles = get_files_with_ext(**COMPILED_FILES['model'])
    modelAssembler.assemble(modelFiles)
    finalSource = matAssembler.outputSource + modelAssembler.outputSource
    if DEBUG:
        with open('generated.glsl', 'w') as file:
            file.write(finalSource)
    return finalSource, modelAssembler.database
