import json
import copy

from rayengine.parser.grouptype import GroupType

OPERATOR_NAME = {'+': 'ADD',
                 '-': 'SUB',
                 '*': 'MULT',
                 '/': 'DIV'}

class AbstractConstruct:

    def __init__(self):
        self.constructs = {}

    def __call__(self, nameInFile=None):
        def decorator(constructClass):
            name = constructClass.__name__ if nameInFile is None else nameInFile
            self.constructs[name] = constructClass
            return constructClass
        return decorator

materialconstruct = AbstractConstruct()
modelconstruct = AbstractConstruct()

def magicreference(constructClass):
    def _build(self):
        self.code = self.args[0]
        return self.code
    constructClass._build = _build
    return constructClass


MATERIAL_MAX_COLOR = 0.3


class Value:
    BUILD_ARGS = True
    ARGS = []
    KWARGS = {}

    def __init__(self, *args, **kwargs):
        self.className = type(self).__name__
        self.args = args
        self.kwargs = kwargs
        self._build()

    def _build(self):
        # process args
        try:
            processor = self.arg_process
        except AttributeError:
            processor = lambda x: x
        newargs = processor(self.args)
        # set default kwargs
        try:
            newkwargs = copy.deepcopy(self.DEFAULT_KWARGS)
        except AttributeError:
            newkwargs = {}
        newkwargs.update(self.kwargs)
        # process kwargs
        processor = self.kwargs_process
        # build args
        build = self.BUILD_ARGS
        if build:
            for arg in newargs:
                arg._build()
            codegetter = lambda arg: arg.code
        else:
            codegetter = lambda arg: arg
        # add generated args from kwargs
        finalargs = [*processor(newkwargs), *[codegetter(arg) for arg in newargs]]
        # generate glsl code
        try:
            glclass = self.GL_CLASS
        except AttributeError:
            glclass = self.className
        if glclass is None:
            res = []
        else:
            res = [glclass]
        if finalargs:
            elem = '('+','.join(finalargs)+')'
            res.append(elem)
        self.code = ''.join(res)
        return self.code

    def kwargs_process(self, kwargs):
        return [value._build() for arg, value in kwargs.items()]

    def operator(self, operator, other):
        try:
            opDefinitions = getattr(self, OPERATOR_NAME[operator])
        except KeyError:
            raise NotImplementedError('Operator {operator} not implemeted for {self.className}')
        return self.__mathop__(operator, opDefinitions, other)

    def extract_types(self, typeTuple):
        return [globals()[item] if type(item) == str else item for item in typeTuple]

    def __mathop__(self, operator, opDefinitions, other):
        for typeTuple in opDefinitions:
            otherType, outputType = self.extract_types(typeTuple)
            if isinstance(other, otherType):
                return outputType(f"({self.code}{operator}{other.code})")
        return other.operator(operator, self)

    def __add__(self, other):
        return self.operator('+', other)

    def __sub__(self, other):
        return self.operator('-', other)

    def __mul__(self, other):
        return self.operator('*', other)

    def __truediv__(self, other):
        return self.operator('/', other)

    def __repr__(self):
        return f"<{self.code}>"

class StringValue(Value):
    ARGS = [str]
    BUILD_ARGS = False
    GL_CLASS = None

    @property
    def value(self):
        return self.args[0]

class FloatValue(Value):

    ADD = [('FloatValue', 'ComposedFloat')]
    SUB = [('FloatValue', 'ComposedFloat')]
    MULT = [('FloatValue', 'ComposedFloat')]


class LiteralFloat(FloatValue):
    ARGS = [float]
    BUILD_ARGS = False
    GL_CLASS = None

    def compute(self):
        return float(self.args[0])

class ManualFloat(FloatValue):
    ARGS = [float]
    BUILD_ARGS = False
    GL_CLASS = None

    def arg_process(self, args):
        return [str(arg) for arg in args]

class ComposedFloat(FloatValue):
    ARGS = [str]
    BUILD_ARGS = False
    GL_CLASS = None

@magicreference
class FloatRef(FloatValue):
    pass

@materialconstruct()
class Sin(FloatValue):
    '''Sin(float)'''
    ARGS = [FloatValue]
    BUILD_ARGS = True

    GL_CLASS = 'sin'

@modelconstruct()
@materialconstruct()
class Vec3(Value):
    '''Vec3(float, float, float)'''
    ARGS = [FloatValue, FloatValue, FloatValue]

    GL_CLASS = 'vec3'

    def get_field(self, field):
        if field in ('x', 'r'):
            return self.args[0]
        elif field in ('y', 'g'):
            return self.args[1]
        elif field in ('z', 'b'):
            return self.args[2]
        else:
            raise ValueError(f'Invalid field name {field} for {self.className}')

@magicreference
class Vec3Ref(Vec3):

    def get_field(self, field):
        if field in ('r', 'g', 'b', 'x', 'y', 'z'):
            return FloatRef(f'{self.code}.{field}')
        else:
            raise ValueError(f'Invalid field name {field} for {self.className}')


@materialconstruct()
class Smoothstep(FloatValue):
    '''Smoothstep(edge0, edge1, value)'''
    ARGS = [FloatValue, FloatValue, FloatValue]

    GL_CLASS = 'smoothstep'

class Color(Value):
    '''Color("#ffffff")'''


    ADD = [('Color', 'ColorComposed')]
    MULT = [(FloatValue, 'ColorComposed')]


@materialconstruct('Color')
class LiteralColor(Color):
    ARGS = [StringValue]
    GL_CLASS = 'vec3'

    def arg_process(self, args):
        htmlColor = args[0].value
        if len(htmlColor) != 7 or htmlColor[0] != '#':
            raise ValueError(f'Invalid color literal "{htmlColor}"')
        return [ManualFloat(MATERIAL_MAX_COLOR*int(htmlColor[i:i+2], 16)/255) for i in range(1, 7, 2)]

@magicreference
class ColorComposed(Color):

    def arg_process(self, args):
        return args

@materialconstruct()
class ColorMix(Color):
    '''ColorMix(colorA, colorB, mixFactor)'''
    ARGS = [Color, Color, FloatValue]
    GL_CLASS = 'mix'

@materialconstruct()
class Material(Value):
    '''Material(color)[metalic]'''
    ARGS = [Color]
    KWARGS = {'metalic': FloatValue}
    DEFAULT_KWARGS = {'metalic': ManualFloat(8)}

class MaterialRef(Value):
    BUILD_ARGS = False
    GL_CLASS = None

    def arg_process(self, args):
        return [f"#{args[0]}#"]

class SDF3(Value):
    KWARGS = {'material': MaterialRef,
              'position': Vec3}

    DEFAULT_KWARGS = {'material': MaterialRef(StringValue('default')),
                      'position': Vec3(*[ManualFloat(0) for _ in range(3)])}

    def kwargs_process(self, kwargs):
        material = kwargs['material'].code
        pos = kwargs['position'].code
        newpos = f'point-{pos}'
        return [newpos, material]

    def __add__(self, other):
        if isinstance(other, SDF3):
            return SDF3Composed(f'Union({self.code}, {other.code})')

@magicreference
class SDF3Composed(SDF3):
    pass

@modelconstruct()
class Sphere(SDF3):
    '''Sphere(radius)'''
    ARGS = [FloatValue]

@modelconstruct()
class Cylinder(SDF3):
    '''Cylinder(height, radius)'''
    ARGS = [FloatValue, FloatValue]

@modelconstruct()
class Cone(SDF3):
    '''Cone(sin, cos, height)'''
    ARGS = [FloatValue, FloatValue, FloatValue]

@modelconstruct()
class Box(SDF3):
    '''Box(width, length, height)'''
    ARGS = [FloatValue, FloatValue, FloatValue]

@modelconstruct()
class PiecewiseHeightmap(SDF3):
    '''PiecewiseHeightmap(amplitude, wavelength)'''
    ARGS = [FloatValue, FloatValue]

    def kwargs_process(self, kwargs):
        args = super().kwargs_process(kwargs)
        return [*args, 'camDist', 'ray']

@modelconstruct()
class Forest(SDF3):
    '''Forest(gridSize)'''
    ARGS = [FloatValue]

@modelconstruct()
class SphereNoiseMountain(SDF3):
    '''SphereNoiseMountain(amplitude)'''
    ARGS = [FloatValue]

    def kwargs_process(self, kwargs):
        args = super().kwargs_process(kwargs)
        return [*args, 'camDist']

@modelconstruct()
class Header(Value):
    '''Header[
        boundType: "sphere" | "plane"
        boundValue: float (radius of the sphere or height of the plane)
    ]'''
    KWARGS = {'boundType': StringValue, 'boundValue': FloatValue}


class Builder:

    def __init__(self, interpreter, constructors):
        modelEval = {
            # GroupType.INSTRUCTION: {
                GroupType.EQUAL: self.register_variable,
            # }
        }
        constructors[GroupType.NUM] = LiteralFloat
        constructors[GroupType.STRING] = StringValue

        operators = {
            GroupType.ADD: self.add,
            GroupType.SUB: self.sub,
            GroupType.MULT: self.mult
        }
        self.interpreter = interpreter
        self.interpreter.define_syntax(program=modelEval, constructors=constructors,
                                       mathOperators=operators)

    def run(self):
        self.interpreter.run()
        self.variables = self.interpreter.get_variables()

    def add(self, valueA, valueB):
        try:
            return valueA + valueB
        except TypeError:
            self.interpreter.error(f'Impossible to add "{valueA}" with "{valueB}"',
                                   self.currentVar)

    def sub(self, valueA, valueB):
        try:
            return valueA - valueB
        except TypeError:
            self.interpreter.error(f'Impossible to substract "{valueA}" - "{valueB}"',
                                   self.currentVar)

    def mult(self, valueA, valueB):
        try:
            return valueA * valueB
        except TypeError:
            self.interpreter.error(f'Impossible to multiply "{valueA}" with "{valueB}"',
                                   self.currentVar)

    def register_variable(self):
        name = self.interpreter.read_var_name()
        if self.interpreter.variable_exist(name):
            self.interpreter.error(f'Redefinition of variable "{name}"', name)
        self.currentVar = name
        value = self.interpreter.read_value(GroupType.VALUE)
        self.interpreter.define_variable(name, value)

    def save_as_json(self, content, destination):
        with open(destination, 'w') as file:
            json.dump(content, file)

class MaterialBuilder(Builder):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs, constructors=materialconstruct.constructs)

    def run(self):
        self.interpreter.define_variable('point', Vec3Ref('point'))
        self.interpreter.define_variable('normal', Vec3Ref('normal'))
        super().run()

    def save_result(self, destination):
        try:
            material = self.variables['material']
        except KeyError:
            self.interpreter.error('No "material" key defined', None)
        content = {'code': material.code}
        self.save_as_json(content, destination)

class ModelBuilder(Builder):

    MODEL_BOUNDS = ['sphere', 'plane']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs, constructors=modelconstruct.constructs)
        self._header = None

    def run(self):
        super().run()

    @property
    def header(self):
        if self._header:
            return self._header
        return self.safe_readkey('header')

    def safe_readkey(self, key, inHeader=False):
        if inHeader:
            try:
                return self.header.kwargs[key]
            except KeyError:
                self.interpreter.error(f'No "{key}" key defined in header', 'header')
        else:
            try:
                return self.variables[key]
            except KeyError:
                self.interpreter.error(f'No "{key}" key defined', None)

    def save_result(self, destination):
        model = self.safe_readkey('model')
        boundType = self.safe_readkey('boundType', inHeader=True)
        boundValue = self.safe_readkey('boundValue', inHeader=True).compute()
        try:
            boundTypeID = self.MODEL_BOUNDS.index(boundType.value)
        except ValueError:
            self.interpreter.error(f'Invalid boundType {boundType}', 'header')
        data = {'boundType': boundTypeID, 'boundValue': boundValue}
        content = {'code': model.code, 'data': data}
        self.save_as_json(content, destination)

