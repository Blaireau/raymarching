from collections import deque

from rayengine.render import Render
from rayengine.entities import Camera, Model
from rayengine.entities.origin import Origin
from rayengine.modelfactory.assembler import assemble_models

from rayengine.systools.default import *

class World:

    def __init__(self):
        source, database = assemble_models()
        self.render = Render(source)
        Model.MODEL_DATABASE = database
        self.origin = Origin()
        self.camera = Camera()
        self.camera.attach_to(self.origin)
        self.lights = []

    def start(self):
        self.render.start(world=self)

    def add_light(self, light):
        self.lights.append(light)

    def get_lights_data(self):
        return [light.get_data() for light in self.lights]

    def get_node_index(self, node):
        try:
            nodeIndex = self.nextIndex[node.nodetype]
        except KeyError:
            return None
        self.nextIndex[node.nodetype] += 1
        return nodeIndex

    def get_geometry(self):
        # first pass : we compute each entities position
        models = []
        nodes_to_parse = deque(self.origin.get_childrens())
        while len(nodes_to_parse) > 0:
            currentNode = nodes_to_parse.popleft()
            currentNode.compute_render_data()
            if currentNode.nodetype == 'Model':
                models.append(currentNode)
            childrens = currentNode.get_childrens()
            nodes_to_parse.extend(childrens)
        modelData = [model.get_model_data() for model in models]
        return modelData

    def on_update(self, *args, **kwargs):
        '''Define this method to avoid errors if the user does not define them'''
        pass

    def on_key_press(self, *args, **kwargs):
        '''Define this method to avoid errors if the user does not define them'''
        pass

    def on_key_release(self, *args, **kwargs):
        '''Define this method to avoid errors if the user does not define them'''
        pass

    def on_mouse_motion(self, x, y, dx, dy):
        '''Define this method to avoid errors if the user does not define them'''
        self.camera.update_rotation(dx, dy)
