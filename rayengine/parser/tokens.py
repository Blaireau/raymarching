from rayengine.parser.chartype import CharType


class Token:
    """
    reprsent an element in the text
    """

    def __init__(self, value, chartype, line, column):
        self.value = value
        self.type = chartype
        self.line = line
        self.column = column

    def __repr__(self):
        return f"<Token:{repr(self.value)}({self.type})>"

    def disp(self):
        return f"{repr(self.value):20} {self.type.name:10} [{self.line}:{self.column}]"

    def __iter__(self):
        return iter([self])

    @classmethod
    def fuse_tokens(cls, tokenList):
        newValue = ''.join((t.value for t in tokenList))
        newType = CharType.NONE
        [newType := t.type | newType for t in tokenList]
        line = tokenList[-1].line
        column = tokenList[-1].column
        return cls(newValue, newType, line, column)


