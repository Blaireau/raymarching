import re

from rayengine.parser.chartype import CharType
from rayengine.parser.tokens import Token


NUMBER_REGEX = [
        r"\d+$", #integer
        r"\d+\.\d+$", #float
]

class SourceParser:
    """
    parse a file a convert it into a list of token objects
    """

    def __init__(self, sourceString, errorManager):
        self.errorManager = errorManager
        self.parse_model_data(sourceString)

    def parse_model_data(self, modelData):
        self.tokens = []
        self.lineCounter = 0
        self.charCounter = 0
        self.clear_buffer()
        self.flushToVoid = False
        self.unconditionalPush = False
        self.expectCharType = None
        for char in '\n'+modelData+'\n':
            self.process_char(char)
        # post processing
        # merge newlines together
        if len(self.tokens) >= 2:
            newTokens = [self.tokens[0]]
            for token in self.tokens[1:]:
                previousToken = newTokens[-1]
                if previousToken.type & token.type & CharType.NEWLINE:
                    newTokens[-1] = Token(previousToken.value+token.value, CharType.NEWLINE,
                                          previousToken.line, previousToken.column)
                else:
                    newTokens.append(token)
        else:
            newTokens = self.tokens
        # remove firt and last tokens if they are newlines
        if newTokens and newTokens[0].type & CharType.NEWLINE:
            newTokens.pop(0)
        if newTokens and newTokens[-1].type & CharType.NEWLINE:
            newTokens.pop()
        self.tokens = newTokens

    def increment_cursor_position(self, chartype):
        self.bufferPosition = (self.lineCounter, self.charCounter)
        if chartype & CharType.NEWLINE:
            self.lineCounter += 1
            self.charCounter = 0
        else:
            self.charCounter += 1

    def process_char(self, char):
        # get char type and move cursor
        chartype = CharType.get_type(char, len(self.buffer))
        self.increment_cursor_position(chartype)
        # handle special modes for comment deletion and strings
        if self.flushToVoid:
            if chartype & self.expectCharType:
                self.flushToVoid = False
            else: #discard comments
                return
        if self.unconditionalPush:
            if chartype & self.expectCharType:
                self.unconditionalPush = False
                self.flush_buffer()
            else:
                self.push_to_buffer(char, CharType.STRING)
            return
        # normal operation
        commontype = self.bufferType & chartype
        isNewType = commontype == CharType.NONE
        isSoloToken = self.bufferType & (CharType.OPERATOR | CharType.OPEN_GROUP | CharType.CLOSE_GROUP)
        if len(self.buffer) > 0 and (isNewType or isSoloToken):
            self.flush_buffer()
            chartype = CharType.get_type(char, 0)
        self.push_to_buffer(char, chartype)
        # handle special cases
        if chartype & CharType.STRING:
            self.unconditionalPush = True
            self.expectCharType = CharType.STRING
            self.clear_buffer()
        if chartype & CharType.COMMENT:
            self.flushToVoid = True
            self.expectCharType = CharType.NEWLINE
            self.clear_buffer()
        if chartype & CharType.SPACE:
            self.clear_buffer()
        # if chartype & CharType.NEWLINE:
        #     self.clear_buffer()

    def push_to_buffer(self, char, chartype):
        self.bufferType &= chartype
        self.buffer.append(char)

    def flush_buffer(self):
        word = ''.join(self.buffer)
        chartype = self.get_final_buffer_type(word)
        newToken = Token(word, chartype, *self.bufferPosition)
        if not chartype:
            self.errorManager(self._error, newToken)
        self.clear_buffer()
        self.tokens.append(newToken)

    def syntax_error(self, detail):
        self._error = f'Syntax error: {detail}'

    def is_number_literal(self, word):
        for test_regex in NUMBER_REGEX:
            if re.match(test_regex, word):
                return True
        return False

    def get_final_buffer_type(self, word):
        self.syntax_error(f"invalid character '{word}'")
        if self.bufferType & CharType.NUM:
            if self.is_number_literal(word):
                return CharType.NUM
            self.syntax_error(f"invalid number '{word}'")
            self.bufferType = CharType.remove_flag(self.bufferType, CharType.NUM)
        if self.bufferType & CharType.OPERATOR:
            if len(self.buffer) != 1:
                self.syntax_error(f"invalid token '{word}'")
                return CharType.NONE
        return self.bufferType

    def clear_buffer(self):
        self.bufferType = CharType.ALL
        self.buffer = []
