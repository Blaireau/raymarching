from rayengine.parser.grouptype import GroupType
from rayengine.parser.tree import Tree

class Variable:

    def __init__(self, token, defined=True):
        self.token = token
        self.defined = defined
        self.value = None


class Interpreter:

    def __init__(self, ast, errorManager):
        self.errorManager = errorManager
        self.ast = ast
        self.variables = {}

    def run(self):
        for subTree in self.ast:
            self.interprete_tree(subTree, self.program, self.constructors,
                                 self.mathOperators)

    def interprete_tree(self, tree, program, constructors, mathOperators):
        childNode = tree[0]
        try:
            newProgram = program[tree.type]
        except KeyError:
            self.errorManager(f'Undefined operation {tree.type}', childNode)
        if isinstance(newProgram, dict):
            # continue on firt child
            self.interprete_tree(childNode, newProgram, constructors, mathOperators)
        else:
            self.lastTree = iter(tree)
            newProgram()

    def read_constructor_value(self, tree):
        constructorToken = tree[0]
        constructorName = constructorToken.value
        try:
            constructor = self.constructors[constructorName]
        except KeyError:
            self.errorManager(f'Undefined constructor "{constructorName}"', constructorToken)
        args, kwargs = self.read_args_kwargs(tree[1:], constructor, constructorToken)
        try:
            res = constructor(*args, **kwargs)
        except ValueError as message:
            self.errorManager(message.args[0], constructorToken)
        return res

    def read_args_kwargs(self, tree, constructor, constructorToken):
        args = []
        kwargs = {}
        subTree = tree[0]
        if len(tree) == 0:
            return args, kwargs
        if subTree.type & GroupType.PARENTHESIS:
            args = self.read_args(subTree, constructor, constructorToken)
            if len(tree) == 1:
                return args, kwargs
            subTree = tree[1]
        if subTree.type & GroupType.BRACKETS:
            kwargs = self.read_kwargs(subTree, constructor.KWARGS)
        return args, kwargs

    def read_args(self, tree, constructor, constructorToken):
        expLength = len(constructor.ARGS)
        expectableLength = len(tree)
        if expectableLength != expLength:
            self.errorManager(f'Constructor {constructorToken.value} needs {expLength} arguments'
                              f' but {expectableLength} where provided.', constructorToken,
                              hint=constructor)
        args = []
        for elem, expectation in zip(tree, constructor.ARGS):
            value = self._read_value(elem, GroupType.VALUE, expectation)
            args.append(value)
        return args

    def read_kwargs(self, tree, expectedTypes):
        kwargs = {}
        for setvalTree in tree:
            name = setvalTree[0].value
            try:
                expectation = expectedTypes[name]
            except KeyError:
                self.errorManager(f'Invalid optional parameter {name}', setvalTree[0])
            value = self._read_value(setvalTree[1], GroupType.VALUE, expectation)
            kwargs[name] = value
        return kwargs

    def math_operator(self, tree, operatorGroup):
        res = self._read_value(tree[0], GroupType.VALUE)
        try:
            operator = self.mathOperators[operatorGroup]
        except KeyError:
            self.errorManager(f'Undefined operator', tree)
        for elem in tree[1:]:
            val = self._read_value(elem, GroupType.VALUE)
            res = operator(res, val)
        return res

    def read_field_component(self, tree):
        if len(tree) != 2:
            self.errorManager(f'Expecting a field name', tree)
        value = self._read_value(tree[0], GroupType.VALUE)
        fieldName = tree[1]
        if not fieldName.type & GroupType.IDENTIFIER:
            self.errorManager(f'Expecting a field name but found {fieldName.value}', fieldName)
        name = fieldName.value
        try:
            fieldgetter = value.get_field
        except AttributeError:
            className = value.__class__.__name__
            self.errorManager(f'Field access is not defined for "{className}"', fieldName)
        res = fieldgetter(name)
        return res

    def read_var_name(self):
        self.lastToken = next(self.lastTree)
        if self.lastToken.type & GroupType.IDENTIFIER:
            name = self.lastToken.value
            self.variables[name] = Variable(self.lastToken, False)
            return name
        else:
            self.errorManager(f'Expecting an identifier', self.lastToken)

    def read_value(self, itemType):
        return self._read_next_value(self.lastTree, itemType)

    def _read_next_value(self, iterable, *args, **kwargs):
        try:
            self.lastToken = next(iterable)
        except StopIteration:
            self.errorManager(f'Expecting a value', self.lastToken)
        return self._read_value(self.lastToken, *args, **kwargs)

    def _read_value(self, token, itemType, expectedConstructor=None):
        self.lastToken = token
        match = token.type & itemType
        if match:
            if match & GroupType.IDENTIFIER:
                name = token.value
                if self.variable_exist(name):
                    val = self.variables[name].value
                else:
                    self.errorManager(f'Undefined variable "{name}"', token)
            elif match & GroupType.CONSTRUCTOR:
                val = self.read_constructor_value(token)
            elif match & GroupType.FIELD_ACCESS:
                val = self.read_field_component(token)
            elif match & GroupType.PARENTHESIS:
                val = self._read_value(iter(token), itemType)
            elif match & (GroupType.ADD | GroupType.MULT | GroupType.SUB | GroupType.DIV):
                val = self.math_operator(token, match)
            else:
                try:
                    constructor = self.constructors[token.type]
                except KeyError:
                    self.errorManager(f'Invalid literal "{token.value}"',
                                      token)
                val = constructor(token.value)
            if expectedConstructor and not isinstance(val, expectedConstructor):
                self.errorManager(f'Expecting a value of type {expectedConstructor.__name__}',
                                  token)
            return val
        else:
            self.errorManager(f'Invalid value "{token.type}" '
                              f'for expected type {itemType}', token)

    def error(self, message, faultyVarName):
        if faultyVarName is None:
            self.errorManager(message, None)
        else:
            var = self.variables[faultyVarName]
            self.errorManager(message, var.token)

    def define_syntax(self, program, constructors, mathOperators):
        self.program = program
        self.constructors = constructors
        self.mathOperators = mathOperators

    def define_variable(self, name, value):
        try:
            var = self.variables[name]
        except KeyError:
            var = Variable(None)
            self.variables[name] = var
        var.value = value
        var.defined = True

    def variable_exist(self, name):
        try:
            var = self.variables[name]
        except KeyError:
            return False
        return var.defined

    def get_variables(self):
        return {name: variable.value for name, variable in self.variables.items()}

