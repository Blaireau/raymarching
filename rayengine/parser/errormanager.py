

class ParseError(Exception):
    '''Raised when an error is detected in a file to parse
    e.g. invalid syntax, type mismatch, illegal name'''




class ErrorManager:

    def __init__(self, header, sourceCode):
        self.header = header
        self.source = sourceCode.split('\n')
        self.boxChar = '─│┌┐└┘'

    def __call__(self, message, faultyToken, hint=None):
        if faultyToken is None:
            displayLines = [self.header, message]
        else:
            location = f"line {faultyToken.line} | column {faultyToken.column}"
            extract = self.source[faultyToken.line - 1]
            pointer = "^".rjust(faultyToken.column)
            displayLines = [self.header, location, extract, pointer, message]
        if hint and hint.__doc__ is not None:
            displayLines.append('Hint:')
            displayLines.append('    '+hint.__doc__)
        message = self.draw_in_box(displayLines)
        raise ParseError(message)

    def draw_in_box(self, lines):
        output = []
        blocLength = max((len(line) for line in lines))
        boxTopLine = self.boxChar[2] + self.boxChar[0]*(blocLength + 2) + self.boxChar[3]
        boxBotLine = self.boxChar[4] + self.boxChar[0]*(blocLength + 2) + self.boxChar[5]
        output.append(boxTopLine)
        char = self.boxChar[1]
        for line in lines:
            output.append(f'{char} {line.ljust(blocLength)} {char}')
        output.append(boxBotLine)
        return '\n'+'\n'.join(output)
