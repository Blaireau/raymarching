from rayengine.parser.tokens import Token
from rayengine.parser.grouptype import GroupType



class Tree:

    def __init__(self, nodes, groupType=GroupType.NONE, line=-1, column=-1):
        self.value = None
        self._groupType = groupType
        self.nodes = nodes
        self.line = line
        self.column = column

    def get_first_token(self):
        for node in self.iter_in_order():
            if isinstance(node, Token):
                return node

    @property
    def type(self):
        return self._groupType

    @type.setter
    def type(self, newType):
        if self._groupType == GroupType.NONE:
            self._groupType = newType

    def apply_on_all(self, *, nodeOp=None, leafOp=None):
        for nid, node in enumerate(self.nodes):
            if type(node) == Tree:
                node.apply_on_all(nodeOp=nodeOp, leafOp=leafOp)
            elif type(node) == Token:
                if leafOp:
                    leafOp(node)
            else:
                raise ValueError(f'Tree contains something else than Token or Tree: {self.nodes}')
        if nodeOp:
            self.apply_on_root(operation=nodeOp)

    def apply_on_root(self, *, operation=None):
        if operation:
            operation(self)

    def iter_in_order(self):
        yield self
        for node in self.nodes:
            if isinstance(node, Tree):
                yield from node.iter_in_order()
            else:
                yield node

    def clear_undefined(self):
        newContent = []
        for nid, node in enumerate(self.nodes):
            if type(node) == Tree:
                node.clear_undefined()
                if node.type == GroupType.NONE:
                    newContent.extend(node.nodes)
                else:
                    newContent.append(node)
            else:
                newContent.append(node)
        self.nodes = newContent

    def __getitem__(self, key):
        return self.nodes[key]

    def __setitem__(self, key, val):
        self.nodes[key] = val

    def __iter__(self):
        return iter(self.nodes)

    def __len__(self):
        return len(self.nodes)

    def show(self, depth=0):
        blank = '│   '
        doted = '├───'
        if depth == 0:
            treeline = ''
        elif depth == 1:
            treeline = doted
        else:
            treeline = blank*(depth-1)+doted
        print(f'{treeline}Tree ({self.type.name}):')
        for node in self.nodes:
            if type(node) == Tree:
                node.show(depth+1)
            else:
                print(f'{blank*depth}{doted}{node}')

