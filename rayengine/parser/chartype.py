from enum import Flag
import string


class CharType(Flag):
    NONE = 0
    IDENTIFIER = 1
    IDENTIFIER_BEGIN = 2
    NUM_BEGIN = 4
    NUM = 8
    NUM_END = 16
    OPEN_GROUP = 32
    CLOSE_GROUP = 64
    STRING = 128
    COMMENT = 256
    SPACE = 512
    NEWLINE = 1024
    OPERATOR = 2048
    CONSTRUCTOR = 4096
    CONSTRUCTOR_BEGIN = 8192
    ALL = 16384 - 1

    @classmethod
    def get_type(cls, char, index):
        finalCharType = cls.NONE
        for (charset, chartype) in CHAR_TYPES:
            if char in charset:
                finalCharType |= chartype
        if index == 0:
            if finalCharType & cls.IDENTIFIER and not finalCharType & cls.IDENTIFIER_BEGIN:
                finalCharType = cls.remove_flag(finalCharType, cls.IDENTIFIER)
            if finalCharType & cls.NUM and not finalCharType & cls.NUM_BEGIN:
                finalCharType = cls.remove_flag(finalCharType, cls.NUM)
            if (    finalCharType & cls.CONSTRUCTOR
                and not finalCharType & cls.CONSTRUCTOR_BEGIN):
                finalCharType = cls.remove_flag(finalCharType, cls.CONSTRUCTOR)
        for transitionFlag in [cls.IDENTIFIER_BEGIN, cls.NUM_BEGIN, cls.CONSTRUCTOR_BEGIN]:
            finalCharType = cls.remove_flag(finalCharType, transitionFlag)
        return finalCharType

    @classmethod
    def remove_flag(cls, chartype, flag):
        return chartype & ~flag

    @classmethod
    def reciprocal(cls, char):
        try:
            return RECIPROCAL_CHAR[char]
        except KeyError:
            return char



CHAR_TYPES = [
    (set(string.ascii_letters+'_'+string.digits), CharType.IDENTIFIER | CharType.CONSTRUCTOR),
    (set(string.ascii_lowercase+'_'),             CharType.IDENTIFIER_BEGIN),
    (set(string.digits), CharType.NUM_BEGIN),
    (set(string.digits+'.'), CharType.NUM),
    (set(string.digits), CharType.NUM_END),
    (set('[({'), CharType.OPEN_GROUP),
    (set('])}'), CharType.CLOSE_GROUP),
    (set('\'"'), CharType.STRING),
    (set('#'), CharType.COMMENT),
    (set(' '), CharType.SPACE),
    (set('\n'), CharType.NEWLINE),
    (set('/*+-:@,=.'), CharType.OPERATOR),
    (set(string.ascii_uppercase), CharType.CONSTRUCTOR_BEGIN),
]

RECIPROCAL_CHAR = {
    '(': ')',
    ')': '(',
    '[': ']',
    ']': '[',
    '{': '}',
    '}': '{',
    '>': '<',
    '<': '>'
}

