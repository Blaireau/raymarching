from enum import Flag
import string


class GroupType(Flag):
    NONE = 0
    PARENTHESIS = 1
    BRACKETS = 2
    ADD = 4
    SUB = 8
    MULT = 16
    DIV = 32
    MODIFIER = 64
    SETVAL = 128
    EQUAL = 256
    IDENTIFIER  = 512
    CURRLY = 1024
    INSTRUCTION = 2048
    CONSTRUCTOR = 4096
    NUM = 8192
    STRING = 16_384
    FIELD_ACCESS = 32_768
    VALUE = (   ADD
              | SUB
              | MULT
              | DIV
              | CONSTRUCTOR
              | STRING
              | NUM
              | IDENTIFIER
              | FIELD_ACCESS
              | PARENTHESIS)

    @classmethod
    def get_group(cls, value):
        try:
            return GROUP_TYPE[value]
        except KeyError:
            return cls.NONE


GROUP_TYPE = {
    '(': GroupType.PARENTHESIS,
    ')': GroupType.PARENTHESIS,
    '[': GroupType.BRACKETS,
    ']': GroupType.BRACKETS,
    '{': GroupType.CURRLY,
    '}': GroupType.CURRLY,
    '+': GroupType.ADD,
    '-': GroupType.SUB,
    '*': GroupType.MULT,
    '/': GroupType.DIV,
    ':': GroupType.SETVAL,
    '=': GroupType.EQUAL,
    '.': GroupType.FIELD_ACCESS,
    '@': GroupType.MODIFIER,
}
