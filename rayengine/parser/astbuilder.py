from rayengine.parser.chartype import CharType
from rayengine.parser.grouptype import GroupType
from rayengine.parser.tokens import Token
from rayengine.parser.tree import Tree


class ASTBuilder:

    def __init__(self, inputTokens, errorManager):
        self.errorManager = errorManager
        self.tree = self.build_tree(inputTokens)
        self.tree.apply_on_root(operation=self.split_on(CharType.NEWLINE))
        self.tree.apply_on_all(nodeOp=self.group_with_operator('='))
        self.tree.apply_on_all(nodeOp=self.split_on(CharType.NEWLINE))
        self.tree.apply_on_all(nodeOp=self.group_constructors)
        self.tree.apply_on_all(nodeOp=self.group_with_operator(':'))
        self.tree.apply_on_all(nodeOp=self.split_on(CharType.OPERATOR, value=','))
        self.tree.apply_on_all(nodeOp=self.split_on(CharType.OPERATOR, value='+',
                                                    groupType=GroupType.ADD))
        self.tree.apply_on_all(nodeOp=self.merge_negative_numbers)
        self.tree.apply_on_all(nodeOp=self.split_on(CharType.OPERATOR, value='-',
                                                    groupType=GroupType.SUB))
        self.tree.apply_on_all(nodeOp=self.split_on(CharType.OPERATOR, value='/',
                                                    groupType=GroupType.DIV))
        self.tree.apply_on_all(nodeOp=self.split_on(CharType.OPERATOR, value='*',
                                                    groupType=GroupType.MULT))
        self.tree.apply_on_all(nodeOp=self.group_with_operator('.'))
        self.tree.apply_on_all(nodeOp=self.group_with_operator('@'))
        self.tree.apply_on_all(nodeOp=self.filter_out(CharType.NEWLINE))
        self.tree.apply_on_all(leafOp=self.to_group_type)
        self.tree.clear_undefined()

    def group_constructors(self, tree):
        output = []
        buffer = []
        iterTree = iter(tree)
        try:
            while True:
                node = next(iterTree)
                if isinstance(node, Token) and node.type & CharType.CONSTRUCTOR:
                    buffer.append(node)
                    node = next(iterTree)
                    if isinstance(node, Tree) and node.type & GroupType.PARENTHESIS:
                        buffer.append(node)
                        node = next(iterTree)
                    if isinstance(node, Tree) and node.type & GroupType.BRACKETS:
                        buffer.append(node)
                    output.append(Tree(buffer, groupType=GroupType.CONSTRUCTOR))
                    buffer = []
                else:
                    output.append(node)
        except StopIteration:
            if buffer:
                output.append(Tree(buffer, groupType=GroupType.CONSTRUCTOR))
            tree.nodes = output

    def to_group_type(self, token):
        attr = token.type.name
        try:
            token.type = getattr(GroupType, attr)
        except AttributeError:
            self.errorManager('Unexpected token', token)

    def build_tree(self, tokens, groupType=GroupType.NONE, line=1, column=1):
        nodeList = []
        tempBuffer = []
        openToken = None
        level = 0
        for token in tokens:
            if token.type & CharType.OPEN_GROUP:
                level += 1
                if openToken:
                    tempBuffer.append(token)
                else:
                    openToken = token
            elif token.type & CharType.CLOSE_GROUP:
                level -= 1
                if openToken is None:
                    self.errorManager('Unexpected closing token.', token)
                if level == 0:
                    if CharType.reciprocal(token.value) != openToken.value:
                        self.errorManager('Invalid closing token.', token)
                    gt = GroupType.get_group(openToken.value)
                    nodeList.append(self.build_tree(tempBuffer, gt,
                                                    line=openToken.line,
                                                    column=openToken.column))
                    tempBuffer = []
                    openToken = None
                else:
                    tempBuffer.append(token)
            else:
                if openToken:
                    tempBuffer.append(token)
                else:
                    nodeList.append(token)
        if openToken:
            self.errorManager('Unmatched opening token.', openToken)
        return Tree(nodeList, groupType, line, column)

    def filter_out(self, chartype):
        def filterfunc(tree):
            buffer = []
            for token in tree:
                if isinstance(token, Token) and token.type & chartype:
                    continue
                else:
                    buffer.append(token)
            tree.nodes = buffer
        return filterfunc


    def split_on(self, chartype, *, value=None, groupType=GroupType.NONE):
        def split(tree):
            output = []
            buffer = []
            for token in tree:
                if (isinstance(token, Token) and token.type & chartype
                    and (not value or token.value == value)):
                    if len(buffer) > 0:
                        output.append(Tree(buffer,
                                           line=token.line, column=token.column))
                        buffer = []
                else:
                    buffer.append(token)
            if len(buffer) > 0:
                output.append(Tree(buffer,
                                   line=token.line, column=token.column))
            if len(output) == 1:
                return # do nothing if there is no split
            tree.nodes = [Tree(output, groupType=groupType)]
        return split

    def merge_negative_numbers(self, tree):
        output = []
        buffer = []
        lastToken = None
        for token in tree:
            if (     isinstance(lastToken, Token)
                 and lastToken.type & CharType.OPERATOR
                 and lastToken.value == '-'
                 and isinstance(token, Token)
                 and token.type & CharType.NUM):
                token.value = '-'+token.value
                token.line = lastToken.line
                token.column = lastToken.column
                buffer.append(token)
                lastToken = None
            else:
                if lastToken:
                    buffer.append(lastToken)
                lastToken = token
        if lastToken:
            buffer.append(lastToken)
        if len(output) == 1:
            return # do nothing if there is no split
        tree.nodes = buffer
        return

    def group_with_operator(self, operator):
        def group(tree):
            split = False
            for splitIndex, token in enumerate(tree):
                if isinstance(token, Token):
                    if token.type & CharType.OPERATOR and token.value == operator:
                        split = True
                        break
            if split:
                part1 = Tree(tree[:splitIndex])
                part2 = Tree(tree[splitIndex+1:])
                newGroup = GroupType.get_group(operator)
                tree.nodes = [Tree([part1, part2], groupType=newGroup,
                                   line=token.line, column=token.column)]
        return group
