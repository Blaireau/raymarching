import pytest

from rayengine.mathtools.quaternions import Quaternion

def test_quaternion():
    iq = Quaternion(0, 1, 0, 0)
    jq = Quaternion(0, 0, 1, 0)
    kq = Quaternion(0, 0, 0, 1)
    assert iq*jq == kq == -jq*iq
    assert jq*kq == iq == -kq*jq
    assert -iq*kq == jq == kq*iq
    assert iq**2 == jq**2 == kq**2 == -1
    aq = Quaternion(1, 2, 3, 4)
    bq = Quaternion(-1, 0, 1, 2)
    assert aq + bq == Quaternion(0, 2, 4, 6)
    assert aq - bq == Quaternion(2, 2, 2, 2)
