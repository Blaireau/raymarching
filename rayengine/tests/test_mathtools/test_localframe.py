import pytest

from math import pi

from rayengine.mathtools import LocalFrame, Vec3, Orientation

def test_localframe():
    loc = LocalFrame()
    orient = Orientation(Vec3(0, 0, 1), angle=pi/2)
    loc.orientation = orient
    loc.origin = Vec3(1, 2, 3)
    res = loc.to_parent_coord(Vec3(1, 0, 0))
    assert res == Vec3(1, 3, 3), res
    loc.rotation = Orientation(Vec3(1, 0, 0), angle=0)
    loc.angular_speed = pi
    assert loc.yaxis == Vec3(-1, 0, 0)
    loc.update(0.5)
    assert loc.yaxis == Vec3(-1, 0, 0)
    assert loc.xaxis == Vec3(0, 0, 1), loc.xaxis
    loc.update(1.5)
    assert loc.yaxis == Vec3(-1, 0, 0)
    assert loc.xaxis == Vec3(0, 1, 0)
    assert loc.xaxis.cross(loc.yaxis) == loc.zaxis
