import pytest

from rayengine.mathtools import Vec3


def test_vector_creation():
    # default vector
    assert Vec3()._values == [0, 0, 0]
    # defined by three numbers
    assert Vec3(1, 2, 3)._values == [1, 2, 3]
    # copy
    assert Vec3(Vec3(1, 2, 3))._values == [1, 2, 3]

def test_vector_eq():
    assert Vec3(3, 4, 5) == Vec3(3.0, 4.0, 5.0)
    assert Vec3(1 - 1.0, 0.3 - 0.2, 0) == Vec3(0, 0.1, 0)

def test_vector_iter():
    assert list(Vec3(6, 3, 2)) == [6, 3, 2]

def test_component():
    v = Vec3(1, 2, 3)
    assert v.x == 1
    assert v.y == 2
    assert v.z == 3

def test_add():
    assert Vec3(1, 2, 3) + Vec3(2, -1, 3) == Vec3(3, 1, 6)

def test_sub():
    assert Vec3(2, -1, 4) - Vec3(3, 3, -3) == Vec3(-1, -4, 7)

def test_neg():
    assert -Vec3(1, 2, -3) == Vec3(-1, -2, 3)

def test_mul():
    assert Vec3(2, 3, 0) * 2 == Vec3(4, 6, 0)

def test_div():
    assert Vec3(1, 4, 8.0) / 2 == Vec3(0.5, 2, 4)

def test_norm():
    assert Vec3().norm() == 0
    assert Vec3(3, 4, 0).norm() == 5
    assert Vec3(0, -9, 0).norm() == 9

def test_unit():
    assert Vec3().unit() == Vec3(0, 0, 0)
    assert Vec3(0, -4, 0).unit() == Vec3(0, -1, 0)

def test_dot_product():
    assert Vec3(1, 2, 3).dot(Vec3(-2, 3, 1)) == 7
    assert Vec3(0, 1, -1).dot(Vec3(9, 1, 1)) == 0

def test_cross_product():
    assert Vec3(1, 2, 3).cross(Vec3(2, 4, 6)) == Vec3(0, 0, 0)
    assert Vec3(1, 0, 0).cross(Vec3(0, 1, 0)) == Vec3(0, 0, 1)
    assert Vec3(0, 1, 0).cross(Vec3(1, 0, 0)) == Vec3(0, 0, -1)
