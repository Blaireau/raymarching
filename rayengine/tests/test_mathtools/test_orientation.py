import pytest

from rayengine.mathtools.orientation import Orientation
from rayengine.mathtools import Orientation, Vec3
from math import pi

def test_rotation_simple_vecteur():
    xaxis = Vec3(1, 0, 0)
    rot = Orientation(Vec3(0, 0, 1), angle=pi/2)
    assert rot.rotate_vector(xaxis) == Vec3(0, 1, 0)

def test_rotation_composition():
    xaxis = Vec3(1, 0, 0)
    rot = Orientation(Vec3(0, 0, 1), angle=pi/2)
    rot2 = Orientation(Vec3(0, 1, 0), angle=pi)
    rotTot = Orientation(Vec3(1, 1, 0), angle=pi)
    assert xaxis.rotated(rotTot) == xaxis.rotated(rot).rotated(rot2)
    res = rot & rot2
    assert xaxis.rotated(rotTot) == xaxis.rotated(res)
    assert res == rotTot, res

def test_no_rotation():
    rot = Orientation(Vec3(0, 0, 1), angle=pi/2)
    noRot = Orientation(Vec3(0, 0, 1), angle=0)
    assert rot & noRot == rot == noRot & rot
