import pytest

from rayengine.entities.entity import Entity

def test_ids():
    ea = Entity()
    eb = Entity()
    ec = Entity()
    eb.attach_to(ea)
    ec.attach_to(eb)
    assert ea._id == 1
    assert eb._id == 2
    assert ec._id == 3

def test_cycle_exception():
    ea = Entity()
    eb = Entity()
    ec = Entity()
    eb.attach_to(ea)
    ec.attach_to(eb)
    with pytest.raises(Exception):
        ea.attach_to(ec)

def test_attach_detach():
    root = Entity()
    ent = Entity()
    child = Entity()
    startId = ent._id
    startChildId = child._id
    child.attach_to(ent)
    ent.attach_to(root)
    ent.detach()
    ent.attach_to(root)
    assert ent._id == startId
    assert child._id == startChildId
    del child
    ent.destroy()
    del ent
    root.destroy()


def test_coord():
    from math import pi
    from rayengine.mathtools import Orientation, Vec3
    ea = Entity()
    eb = Entity()
    ec = Entity()
    eb.attach_to(ea)
    ec.attach_to(eb)
    ea.pos = Vec3(1, 2, 0)
    eb.pos = Vec3(2, 0, 0)
    ec.pos = Vec3(2, 2, 0)
    eb.orientation = Orientation(Vec3(0, 0, 1), angle=pi/2)
    for e in [ea, eb, ec]:
        e.compute_render_data()
    assert eb._global_pos == Vec3(3, 2, 0), eb._global_pos
    assert ec._global_pos == Vec3(1, 4, 0), ec._global_pos
