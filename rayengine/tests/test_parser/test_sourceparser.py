import pytest

from rayengine.parser.chartype import CharType
from rayengine.parser.tokens import Token
from rayengine.parser.sourceparser import SourceParser
from rayengine.parser.errormanager import ErrorManager, ParseError

def make_test(text, expect, stop=False):
    em = ErrorManager('Error in sourceparser test', text)
    sp = SourceParser(text, em)
    if stop:
        print(sp.tokens)
        breakpoint()
    return compare(expect, sp.tokens)

def compare(expectedTokens, tokens):
    assert len(expectedTokens) == len(tokens)
    for expected, real in zip(expectedTokens, tokens):
        assert expected.value == real.value
        assert expected.type & real.type != CharType.NONE
        assert expected.line == real.line
        assert expected.column == real.column

def test_empty():
    text = ''
    expect = []
    make_test(text, expect)

def test_simple():
    text = 'a = 5'
    expect = [Token('a', CharType.IDENTIFIER, 1, 1),
              Token('=', CharType.OPERATOR, 1, 3),
              Token('5', CharType.NUM, 1, 5)]
    make_test(text, expect)

def test_comment():
    text = 'bonjour = hello + world #I can write whatever here 124 + 34-13@wzçêf'
    expect = [Token('bonjour', CharType.IDENTIFIER, 1, 7),
              Token('=', CharType.OPERATOR, 1, 9),
              Token('hello', CharType.IDENTIFIER, 1, 15),
              Token('+', CharType.OPERATOR, 1, 17),
              Token('world', CharType.IDENTIFIER, 1, 23)]
    make_test(text, expect)

def test_numbers():
    text = '34-289.51+32'
    expect = [Token('34', CharType.NUM, 1, 2),
              Token('-', CharType.OPERATOR, 1, 3),
              Token('289.51', CharType.NUM, 1, 9),
              Token('+', CharType.OPERATOR, 1, 10),
              Token('32', CharType.NUM, 1, 12)]
    make_test(text, expect)

def test_parenthesis():
    text = '3+(4 - 5 - (32 + 12))'
    expect = [Token('3', CharType.NUM, 1, 1),
              Token('+', CharType.OPERATOR, 1, 2),
              Token('(', CharType.OPEN_GROUP, 1, 3),
              Token('4', CharType.NUM, 1, 4),
              Token('-', CharType.OPERATOR, 1, 6),
              Token('5', CharType.NUM, 1, 8),
              Token('-', CharType.OPERATOR, 1, 10),
              Token('(', CharType.OPEN_GROUP, 1, 12),
              Token('32', CharType.NUM, 1, 14),
              Token('+', CharType.OPERATOR, 1, 16),
              Token('12', CharType.NUM, 1, 19),
              Token(')', CharType.CLOSE_GROUP, 1, 20),
              Token(')', CharType.CLOSE_GROUP, 1, 21)]
    make_test(text, expect)

def test_string():
    text = 'a = "this is 1 string * + -"'
    expect = [Token('a', CharType.IDENTIFIER, 1, 1),
              Token('=', CharType.OPERATOR, 1, 3),
              Token('this is 1 string * + -', CharType.STRING, 1, 27)]
    make_test(text, expect)

def test_nasty_multiline_string():
    text = """
a='#
b+3
'#ignore also this comment
3'get the new
string'4#'this is a comment, not a string'
5
"""
    expect = [Token('a', CharType.IDENTIFIER, 2, 1),
              Token('=', CharType.OPERATOR, 2, 2),
              Token('#\nb+3\n', CharType.STRING, 4, 0),
              Token('\n', CharType.NEWLINE, 5, 0),
              Token('3', CharType.NUM, 5, 1),
              Token('get the new\nstring', CharType.STRING, 6, 6),
              Token('4', CharType.NUM, 6, 8),
              Token('\n', CharType.NEWLINE, 7, 0),
              Token('5', CharType.NUM, 7, 1),
              ]
    make_test(text, expect)





