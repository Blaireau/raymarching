from rayengine.parser.tree import Tree
from rayengine.parser.tokens import Token
from rayengine.parser.chartype import CharType

FILL = (CharType.ALL, 0, 0)

def compare(treeA, treeB):
    assert type(treeA) == type(treeB)
    if type(treeA) == Tree:
        assert len(treeA) == len(treeB)
        for subTreeA, subTreeB in zip(treeA, treeB):
            compare(subTreeA, subTreeB)
    else:
        assert type(treeA) == Token
        assert treeA.value == treeB.value

def test_basic():
    size = 20
    lin = list(range(size))
    tree = Tree(lin)
    assert len(tree) == size
    assert list(tree) == lin

def test_compare():
    tok1 = Token(3, *FILL)
    tok2 = Token(2, *FILL)
    ta = Tree([tok1, tok2])
    tok3 = Token(3, *FILL)
    tok4 = Token(2, *FILL)
    tb = Tree([tok3, tok4])
    compare(ta, tb)

def test_no_useless_nesting():
    tok1 = Token(3, *FILL)
    tok2 = Token(2, *FILL)
    tok3 = Token(5, *FILL)
    ta = Tree([tok1, Tree([tok2, tok3])])
    tb = Tree([Tree([Tree([Tree([ta])])])])
    tb.show()
    tb.clear_undefined()
    ta.show()
    tb.show()
    compare(tb, ta)

def test_leaf_operation():
    tok1 = Token(3, *FILL)
    tok2 = Token(2, *FILL)
    tok3 = Token(1 , *FILL)
    tok4 = Token(5, *FILL)
    tok5 = Token(28, *FILL)
    tok6 = Token(1, *FILL)
    treeA = Tree([tok1, tok2])
    treeB = Tree([tok3, tok4])
    treeC = Tree([treeA, treeB, tok5])
    treeD = Tree([treeC, tok6])
    def mult(token):
        token.value *= 2
    treeD.apply_on_all(leafOp=mult)
    assert tok1.value == 6
    assert tok2.value == 4
    assert tok3.value == 2
    assert tok4.value == 10
    assert tok5.value == 56
    assert tok6.value == 2

def test_operation():
    tok1 = Token(3, *FILL)
    tok2 = Token(2, *FILL)
    tok3 = Token(1 , *FILL)
    tok4 = Token(5, *FILL)
    tok5 = Token(28, *FILL)
    tok6 = Token(1, *FILL)
    treeA = Tree([tok1, tok2])
    treeB = Tree([tok3, tok4])
    treeC = Tree([treeA, treeB, tok5])
    treeD = Tree([treeC, tok6])
    def sum_all(node):
        total = sum([t.value for t in node])
        node.value = total
        node.nodes = [Token(total, *FILL)]

    treeD.apply_on_all(nodeOp=sum_all)
    assert treeD.value == 40

