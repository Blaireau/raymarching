import pytest

from rayengine.parser.tokens import Token
from rayengine.parser.tree import Tree
from rayengine.parser.errormanager import ErrorManager, ParseError
from rayengine.parser.chartype import CharType
from rayengine.parser.astbuilder import ASTBuilder
from rayengine.parser.sourceparser import SourceParser


def make_test(text, expect, stop=False):
    em = ErrorManager('Error in astbuilder test', text)
    sp = SourceParser(text, em)
    builder = ASTBuilder(sp.tokens, em)
    if stop:
        breakpoint()
    return compare(expect, builder.tree)

def should_error(text):
    em = ErrorManager('Error in astbuilder test', text)
    sp = SourceParser(text, em)
    with pytest.raises(ParseError) as e_info:
        builder = ASTBuilder(sp.tokens, em)

def compare(treeA, treeB):
    assert type(treeA) == type(treeB)
    if type(treeA) == Tree:
        assert len(treeA) == len(treeB)
        for subTreeA, subTreeB in zip(treeA, treeB):
            compare(subTreeA, subTreeB)
    else:
        assert type(treeA) == Token
        assert treeA.value == treeB.value


FILL = (CharType.ALL, 0, 0)

# GENERATING MACROS

# SETUP (the number of tokens, then the list of tokens separated by spaces)
#        don’t forget a final space at the end

#<n>
#<t0> <t1> … <tn> 

# f : generate one formated step from the input
# uusidnvansCetokþn; = Token('þie', *FILL)þrancddP

# r : run f until the end
# uui;çfþêtaadçt

# t : used as a temp register by r



# def test_flat():
#     text = '''a = b'''
#     a = Token('a', *FILL)
#     eq = Token('=', *FILL)
#     b = Token('b', *FILL)
#     expect = Tree([Tree([Tree([a, b])])])
#     make_test(text, expect, True)

# def test_parenthesis_math():
#     text = '''a = 3*(4-2) + 8'''
#     tok0 = Token('a', *FILL)
#     tok1 = Token('=', *FILL)
#     tok2 = Token('3', *FILL)
#     tok3 = Token('*', *FILL)
#     tok5 = Token('4', *FILL)
#     tok6 = Token('-', *FILL)
#     tok7 = Token('2', *FILL)
#     tok8 = Token('+', *FILL)
#     tok9 = Token('8', *FILL)
#     expect = Tree([Tree([Tree([tok0,
#                         Tree([Tree([tok2, Tree([tok5, tok7])]),
#                                     tok9
#                         ])
#                     ])
#                 ])
#             ])
#     make_test(text, expect)

# def test_structure():
#     text = '''name = Variable(arg, param, etc)'''
#     tok1 = Token('name', *FILL)
#     tok2 = Token('=', *FILL)
#     tok3 = Token('Variable', *FILL)
#     # tok4 = Token('(', *FILL)
#     tok5 = Token('arg', *FILL)
#     tok6 = Token(',', *FILL)
#     tok7 = Token('param', *FILL)
#     tok8 = Token(',', *FILL)
#     tok9 = Token('etc', *FILL)
#     # tok10 = Token(')', *FILL)
#     expect = Tree([Tree([Tree([tok1,
#                         Tree([tok3, Tree([tok5, tok7, tok9])])
#                     ])
#                 ])
#             ])
#     make_test(text, expect, True)

# def test_parenthesis_and_brackets():
#     text = '''var = Class ( 3 ) [ ( 2 + 1 ) ]'''
#     tok1 = Token('var', *FILL)
#     tok2 = Token('=', *FILL)
#     tok3 = Token('Class', *FILL)
#     # tok4 = Token('(', *FILL)
#     tok5 = Token('3', *FILL)
#     # tok6 = Token(')', *FILL)
#     # tok7 = Token('[', *FILL)
#     # tok8 = Token('(', *FILL)
#     tok9 = Token('2', *FILL)
#     tok10 = Token('+', *FILL)
#     tok11 = Token('1', *FILL)
#     # tok12 = Token(')', *FILL)
#     # tok13 = Token(']', *FILL)
#     expect = Tree([Tree([Tree([tok1, Tree([tok3,
#             Tree([tok5]),
#             Tree([
#                 Tree([tok9, tok11])
#                     ])
#                 ])
#             ])
#         ])
#     ])
#     make_test(text, expect)

# def test_multiline():
#     text = '''
# a = 3
# b = a + 2

# c = 20
# '''
#     tok1 = Token('a', *FILL)
#     tok2 = Token('=', *FILL)
#     tok3 = Token('3', *FILL)
#     # tok4 = Token('\n', *FILL)
#     tok5 = Token('b', *FILL)
#     tok6 = Token('=', *FILL)
#     tok7 = Token('a', *FILL)
#     tok8 = Token('+', *FILL)
#     tok9 = Token('2', *FILL)
#     # tok10 = Token('\n\n', *FILL)
#     tok11 = Token('c', *FILL)
#     tok12 = Token('=', *FILL)
#     tok13 = Token('20', *FILL)
#     expect = Tree([
#         Tree([Tree([
#             Tree([tok1, tok3]),
#         ])]),
#         Tree([Tree([
#             Tree([tok5, Tree([tok7, tok9])]),
#         ])]),
#         Tree([Tree([
#             Tree([tok11, tok13])
#         ])])
#     ])
#     make_test(text, expect)

def test_parenthesis_mismatch():
    text = ''' ( ) )'''
    tok1 = Token('(', *FILL)
    tok2 = Token(')', *FILL)
    tok3 = Token(')', *FILL)
    should_error(text)

def test_bracket_mismatch():
    text = '''[ [ ] [ [ ] ]'''
    tok1 = Token('[', *FILL)
    tok2 = Token('[', *FILL)
    tok3 = Token(']', *FILL)
    tok4 = Token('[', *FILL)
    tok5 = Token('[', *FILL)
    tok6 = Token(']', *FILL)
    tok7 = Token(']', *FILL)
    should_error(text)

def test_mixed_bracket_parenthesis():
    text = '''( [ ] ( [ ) ] )'''
    tok1 = Token('(', *FILL)
    tok2 = Token('[', *FILL)
    tok3 = Token(']', *FILL)
    tok4 = Token('(', *FILL)
    tok5 = Token('[', *FILL)
    tok6 = Token(')', *FILL)
    tok7 = Token(']', *FILL)
    tok8 = Token(')', *FILL)
    should_error(text)

