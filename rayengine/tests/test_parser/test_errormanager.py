import pytest

from rayengine.parser.tokens import Token
from rayengine.parser.chartype import CharType
from rayengine.parser.errormanager import ErrorManager, ParseError



def test_error_manager():
    to = Token('bidule', CharType.IDENTIFIER, 1, 1)
    manager = ErrorManager('Test error manager', 'bidule')
    with pytest.raises(ParseError) as e_info:
        manager('invalid name', to)
