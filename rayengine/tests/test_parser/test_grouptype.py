from rayengine.parser.grouptype import GroupType

def test_get_type():
    assert GroupType.get_group('(') & GroupType.PARENTHESIS
    assert GroupType.get_group(']') & GroupType.BRACKETS
    assert GroupType.get_group('-') & GroupType.SUB
    assert GroupType.get_group('.') & GroupType.FIELD_ACCESS

def test_composed_type():
    assert GroupType.VALUE & GroupType.ADD
    assert GroupType.VALUE & GroupType.CONSTRUCTOR
    assert not GroupType.VALUE & GroupType.PARENTHESIS
