from rayengine.parser.chartype import CharType

def test_get_type():
    assert CharType.get_type('t', 0) & CharType.IDENTIFIER
    assert CharType.get_type('3', 0) & CharType.NUM
    assert CharType.get_type('3', 1) & CharType.IDENTIFIER
    assert CharType.get_type('@', 0) & CharType.OPERATOR
    assert CharType.get_type('-', 0) & CharType.OPERATOR
    assert CharType.get_type('.', 0) & CharType.OPERATOR
    assert bool(CharType.get_type('ª', 0)) is False
    assert bool(CharType.get_type('t', 0)) is True

def test_remove_flag():
    a = CharType.IDENTIFIER
    b = CharType.NUM
    assert CharType.remove_flag(a | b, b) == a

    a = CharType.IDENTIFIER
    b = CharType.NUM
    c = CharType.NONE
    assert CharType.remove_flag(a | b, c) == (a | b)

    a = CharType.IDENTIFIER
    b = CharType.NUM
    c = CharType.ALL
    assert CharType.remove_flag(a | b, c) == (CharType.NONE)

    a = CharType.OPEN_GROUP
    b = CharType.NUM
    c = CharType.CLOSE_GROUP
    d = CharType.STRING
    e = a | b | c | d
    assert CharType.remove_flag(e, b | c) == (a | d)

def test_reciprocal():
    assert CharType.reciprocal('(') == ')'
    assert CharType.reciprocal(')') == '('
    assert CharType.reciprocal(']') == '['
    assert CharType.reciprocal('[') == ']'
    assert CharType.reciprocal('a') == 'a'
    assert CharType.reciprocal('42') == '42'
