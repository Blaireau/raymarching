import os
from pathlib import Path

def get_directory_path(folder, make=False):
    path = Path(folder)
    if path.is_dir():
        return path
    path = os.getcwd() / path
    if path.is_dir():
        return path
    else:
        if make:
            path.mkdir()
            return path
        else:
            raise FileNotFoundError(f'Impossible to find the directory {folder}')

def get_files_with_ext(directory, ext):
    path = get_directory_path(directory)
    return list(path.glob('*'+ext))

def clean_directory(directory):
    path = Path(directory)
    if path.is_dir():
        for file in path.iterdir():
            file.unlink()
