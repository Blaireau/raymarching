SOURCE_FILES = {
    'model': {
        'ext': '.model',
        'directory': 'models'
    },
    'material': {
        'ext': '.material',
        'directory': 'materials'
    }
}

COMPILED_FILES = {
    'model': {
        'ext': '.json',
        'directory': '__modelcache__'
    },
    'material': {
        'ext': '.json',
        'directory': '__materialcache__'
    }
}
